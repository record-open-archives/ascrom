/**
  * @file Yield.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace ASCROM {

using namespace vle::discrete_time;

class Yield : public DiscreteTimeDyn
{
public:
Yield (
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    CropWaterStress.init(this, "CropWaterStress", evts);
    getOptions().syncs.insert(std::make_pair("CropWaterStress", 1)); // force sync for in input
    Is_Failure.init(this, "Is_Failure", evts);
    getOptions().syncs.insert(std::make_pair("Is_Failure", 1)); // force sync for in input
    YieldMax.init(this, "YieldMax", evts);
    getOptions().syncs.insert(std::make_pair("YieldMax", 1)); // force sync for in input
    Ky.init(this, "Ky", evts);
    getOptions().syncs.insert(std::make_pair("Ky", 1)); // force sync for in input

    StressReductionFactor.init(this, "StressReductionFactor", evts);
    mYield.init(this, "Yield", evts);
    YieldFraction.init(this, "YieldFraction", evts);

}

virtual ~Yield ()
{}

void compute(const vle::devs::Time& /*t*/)
{
    bool isCrop = ((YieldMax() > 0) & !(bool)Is_Failure());
    if (!isCrop) {
        daycounter = 0;
        StressReductionFactor = 0;
    } else {
        daycounter = daycounter + 1;
        StressReductionFactor = (StressReductionFactor(-1)*(daycounter-1) + CropWaterStress())/daycounter;
    }
    
    if (!isCrop) {
        mYield = 0.;
    } else {
        mYield = std::max(0., YieldMax() * (1. - Ky() * (1. - StressReductionFactor())));
    }
    
    if (!isCrop) {
        YieldFraction = 0.;
    } else {
        YieldFraction = mYield() / YieldMax();
    }
}

    Var CropWaterStress;
    Var Is_Failure;
    Var YieldMax;
    Var Ky;

    Var StressReductionFactor;
    Var mYield;
    Var YieldFraction;

    int daycounter;

};

} // namespace ASCROM

DECLARE_DYNAMICS(ASCROM::Yield)

