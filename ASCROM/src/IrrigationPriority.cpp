/**
  * @file IrrigationPriority.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <numeric>
#include <vle/utils/Tools.hpp> // vle::utils::ModellingError
#include <algorithm> // std::sort

namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace ASCROM {

using namespace vle::discrete_time;

class IrrigationPriority : public DiscreteTimeDyn
{
public:
IrrigationPriority (
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    // parameters
    DbgLog = (evts.exist("DbgLog")) ? vv::toDouble(evts.get("DbgLog")) : 0.;
    nbBele = vv::toInteger(evts.get("nbBele"));
    a = vv::toDouble(evts.get("a"));
    b = vv::toDouble(evts.get("b"));
    c = vv::toDouble(evts.get("c"));
    d = vv::toDouble(evts.get("d"));
    RS_strategy = vv::toInteger(evts.get("RS_strategy"));
    RC_strategy = vv::toInteger(evts.get("RC_strategy"));
    
    const vv::Set& BP = evts.getSet("BelePriorities");
    for (unsigned int index = 0; index < BP.size(); ++index) {
        BelePriorities.push_back(BP.getDouble(index));
    }
    check_unicity(BelePriorities, DbgLog);

    
    if (DbgLog > 0.) {
        std::cout << "[IrrigationPriority] (" << getModel().getCompleteName() << ") Using Parameters :" <<std::endl;
        print_all_parameters();
    }

    if (DbgLog > 0.) {
        std::cout << "\tDeclaring Input/Output variables..." <<std::endl;
    }

    for (int i=0; i < nbBele; i++) {
        R.push_back(generate_Var(evts, "R", i, false, DbgLog));
        (*R[i]).init_value(0.);
    }


    unsigned int rank_r[nbBele];
    ranking_Rs(rank_r, get_VarVector(R), BelePriorities);

    for (int i=0; i < nbBele; i++) {
        // daily inputs
        CropPriority.push_back(generate_Var(evts, "CropPriority", i, true, DbgLog));
        Days_Without_Irrigation.push_back(generate_Var(evts, "Days_Without_Irrigation", i, false, DbgLog));
        Lethal_Threshold_Days_Without_Water.push_back(generate_Var(evts, "Lethal_Threshold_Days_Without_Water", i, true, DbgLog));
        Days_Since_Sowing.push_back(generate_Var(evts, "Days_Since_Sowing", i, false, DbgLog));
        Crop_Cycle_Duration.push_back(generate_Var(evts, "Crop_Cycle_Duration", i, true, DbgLog));
        Irrigation_Technic_Factor.push_back(generate_Var(evts, "Irrigation_Technic_Factor", i, true, DbgLog));

        // state variables
        mIrrigationPriority.push_back(generate_Var(evts, "IrrigationPriority", i, false, DbgLog));
        
        (*mIrrigationPriority[i]).init_value(rank_r[i]);
        
        RE.push_back(generate_Var(evts, "RE", i, false, DbgLog));
        RS.push_back(generate_Var(evts, "RS", i, false, DbgLog));
        RC.push_back(generate_Var(evts, "RC", i, false, DbgLog));
        RT.push_back(generate_Var(evts, "RT", i, false, DbgLog));
    }

}

virtual ~IrrigationPriority ()
{}

void compute(const vle::devs::Time& t)
{
    if (DbgLog > 0.) {
        std::cout << t << "\t[IrrigationPriority] (" << getModel().getCompleteName() << ") Starting daily computations..." <<std::endl;
    }
    if (DbgLog > 1.) {
        std::cout << "\tDaily inputs :" <<std::endl;
        print_all_inputs();
    }

    double previous_max_Irrigation_Technic_Factor = max_Irrigation_Technic_Factor;
    max_Irrigation_Technic_Factor = 0;
    for (int i=0; i < nbBele; i++) {
        max_Irrigation_Technic_Factor = std::max(max_Irrigation_Technic_Factor, (*Irrigation_Technic_Factor[i])());
    }
    
    bool same_R = true;
    for (int i=0; i < nbBele; i++) {
        bool same_RE_inputs = true ;
        for (int j=0; j < nbBele; j++) {
            same_RE_inputs = same_RE_inputs & ((*CropPriority[j])() == (*CropPriority[j])(-1)) ;
        }
        if (!same_RE_inputs) {
            std::vector<double> CPi = get_VarVector(CropPriority);
            double sumCropPriority = std::accumulate(CPi.begin(), CPi.end(), 0);
            *RE[i] = (sumCropPriority>0) ? (*CropPriority[i])() / sumCropPriority : 0.;
            if (DbgLog > 1.) std::cout << "\tUpdating RE value for plot " << i+1 <<": " << (*RE[i])() << std::endl;
        }

        bool same_RS_inputs = ((*Lethal_Threshold_Days_Without_Water[i])() == (*Lethal_Threshold_Days_Without_Water[i])(-1)) & ((*Days_Without_Irrigation[i])(-1) == (*Days_Without_Irrigation[i])(-2));
        if (!same_RS_inputs) {
            if ((*Lethal_Threshold_Days_Without_Water[i])()>0) {
                if (RS_strategy == 1 ) {
                    *RS[i] = std::max(0.,( (*Lethal_Threshold_Days_Without_Water[i])() - (*Days_Without_Irrigation[i])(-1) )) / (*Lethal_Threshold_Days_Without_Water[i])();
                } else if (RS_strategy == 2) {
                    *RS[i] = (*Days_Without_Irrigation[i])(-1) / (*Lethal_Threshold_Days_Without_Water[i])();
                }
            } else {
                *RS[i] = 0;
            }
            if (DbgLog > 1.) std::cout << "\tUpdating RS value for plot " << i+1 <<": " << (*RS[i])() << std::endl;
        }

        bool same_RC_inputs = ((*Crop_Cycle_Duration[i])() == (*Crop_Cycle_Duration[i])(-1)) & ((*Days_Since_Sowing[i])(-1) == (*Days_Since_Sowing[i])(-2));
        if (!same_RC_inputs) {
            if ((*Crop_Cycle_Duration[i])()>0) {
                if (RC_strategy == 1 ) {
                    *RC[i] = std::max(0., (*Crop_Cycle_Duration[i])() - (*Days_Since_Sowing[i])(-1) ) / (*Crop_Cycle_Duration[i])();
                } else if (RC_strategy == 2) {
                    *RC[i] = (*Days_Since_Sowing[i])(-1) / (*Crop_Cycle_Duration[i])();
                }
            } else {
                *RC[i] = 0;
            }
            if (DbgLog > 1.) std::cout << "\tUpdating RC value for plot " << i+1 <<": " << (*RC[i])() << std::endl;
        }

        bool same_RT_inputs = ((*Irrigation_Technic_Factor[i])() == (*Irrigation_Technic_Factor[i])(-1)) & (max_Irrigation_Technic_Factor==previous_max_Irrigation_Technic_Factor);
        if (!same_RT_inputs) {
            *RT[i] = (max_Irrigation_Technic_Factor>0)?(*Irrigation_Technic_Factor[i])() / max_Irrigation_Technic_Factor : 0;
            if (DbgLog > 1.) std::cout << "\tUpdating RT value for plot " << i+1 <<": " << (*RT[i])() << std::endl;
        }

        bool same_R_inputs = ( same_RE_inputs & same_RS_inputs & same_RC_inputs & same_RT_inputs );
        if (!same_R_inputs) {
            *R[i] = a * (*RE[i])() + b * (*RS[i])() + c * (*RC[i])() + d * (*RT[i])();
            if (DbgLog > 1.) std::cout << "\tUpdating R value for plot " << i+1 <<": " << (*R[i])() << std::endl;
        }

        if ( (*R[i])() != (*R[i])(-1) ) {
            same_R = false;
        }
    }

    // ranking values of R_i only if R_i values have changed
    if (!same_R) {
        if (DbgLog > 0.) std::cout << "\tUpdating plot priorities : " << std::endl;
        unsigned int rank_r[nbBele];
        ranking_Rs(rank_r, get_VarVector(R), BelePriorities);
        for(int i=0;i<nbBele;i++) {
            *mIrrigationPriority[i] = rank_r[i];
            if (DbgLog > 0.) std::cout <<  "\t\tplot: " << i+1 << " now with priority order: " << rank_r[i] << "\t(R:" << (*R[i])() << ", and RE:"<< (*RE[i])()<<")" << std::endl;
        }
    }
    
    // print all daily state variables outputs
    if (DbgLog > 1.) {
        std::cout << "\tDaily state variables :" <<std::endl;
        print_all_outputs();
    }

}
    // inputs
    std::vector<Var*> CropPriority;
    std::vector<Var*> Days_Without_Irrigation;
    std::vector<Var*> Lethal_Threshold_Days_Without_Water;
    std::vector<Var*> Days_Since_Sowing;
    std::vector<Var*> Crop_Cycle_Duration;
    std::vector<Var*> Irrigation_Technic_Factor;

    // state variables
    std::vector<Var*> mIrrigationPriority;
    std::vector<Var*> R;
    std::vector<Var*> RE;
    std::vector<Var*> RS;
    std::vector<Var*> RC;
    std::vector<Var*> RT;

    // parameters
    double DbgLog;
    int nbBele;
    double a;
    double b;
    double c;
    double d;
    int RS_strategy;
    int RC_strategy;
    std::vector<double> BelePriorities;

    //local variable
    double max_Irrigation_Technic_Factor;

private:

    Var* generate_Var(const vd::InitEventList& evts, std::string varname_prefix, int i, bool isSync, double dbglog) {
        std::string varname = varname_prefix+"_"+std::to_string(i+1);
        Var* r = new Var();
        r->init(this, varname, evts);
        if (isSync) getOptions().syncs.insert(std::make_pair(varname, 1)); // force sync for in input
        if (dbglog > 1.) std::cout << "\t\tAdding variable:\t" << varname << "\t(sync:" << (isSync?"true":"false") << ")" << std::endl;
        return(r);
    }

    void ranking_Rs(unsigned int rank_order[], std::vector<double> R_values, std::vector<double> BP_values) {
        unsigned int length = R_values.size();
        std::fill(rank_order, rank_order+length, 1);
        for(unsigned int i=0;i<length;i++) {
            for(unsigned int j=0;j<length;j++) {
                bool different_plot = (i != j);
                bool greater_R = (R_values[i] > R_values[j]);
                bool same_R_and_greater_BP = ( ( R_values[i] == R_values[j] ) & ( BP_values[i] > BP_values[j] ) );
                if ( different_plot & ( greater_R | same_R_and_greater_BP ) ) {
                    rank_order[i]++;
                }
            }
        }
    }

    // extract daily values from std::vector<Var*>
    std::vector<double> get_VarVector(std::vector<Var*> VarVector) {
        std::vector<double> doublevector;
        for(unsigned int i=0;i<VarVector.size();i++) {
            doublevector.push_back((*VarVector[i])());
        }
        return(doublevector);
    }
    
    void check_unicity(std::vector<double> values, double dbglog) { // from https://stackoverflow.com/questions/46477764/check-stdvector-has-duplicates/46477901
        std::sort(values.begin(), values.end());
        auto it = std::unique( values.begin(), values.end() );
        bool wasUnique = (it == values.end() );
        if (wasUnique) {
            if (dbglog > 1.) std::cout << "\t\tAll BelePriorities are unique" <<std::endl;
        } else {
            if (dbglog > 1.) std::cout << "\t\tSome BelePriorities are not unique" <<std::endl;
            throw vu::ModellingError(vu::format("[%s] Some BelePriorities are not unique \n",
                    getModel().getCompleteName().c_str()));
        }
    }


    void print_all_parameters() {
        std::cout << "\t\tDbgLog:" << DbgLog << std::endl;
        std::cout << "\t\tnbBele:" << nbBele << std::endl;
        std::cout << "\t\ta:" << a << std::endl;
        std::cout << "\t\tb:" << b << std::endl;
        std::cout << "\t\tc:" << c << std::endl;
        std::cout << "\t\td:" << d << std::endl;
        std::cout << "\t\tRS_strategy:" << RS_strategy << std::endl;
        std::cout << "\t\tRC_strategy:" << RC_strategy << std::endl;
        for (int i=0; i < nbBele; i++) {
            std::cout << "\t\tBelePriority_"<< i+1 << ":\t" << BelePriorities[i] << std::endl;
        }
    } 

    void print_all_inputs() {
        for (int i=0; i < nbBele; i++) {
            std::cout << "\t\tCropPriority_"<< i+1 << "(t):\t" << (*CropPriority[i])() << std::endl;
            std::cout << "\t\tDays_Without_Irrigation_"<< i+1 << "(t):\t" << (*Days_Without_Irrigation[i])()  << "\t(day)"<< std::endl;
            std::cout << "\t\tLethal_Threshold_Days_Without_Water_"<< i+1 << "(t):\t" << (*Lethal_Threshold_Days_Without_Water[i])()  << "\t(-)"<< std::endl;
            std::cout << "\t\tDays_Since_Sowing_"<< i+1 << "(t):\t" << (*Days_Since_Sowing[i])() << std::endl;
            std::cout << "\t\tCrop_Cycle_Duration_"<< i+1 << "(t):\t" << (*Crop_Cycle_Duration[i])() << std::endl;
            std::cout << "\t\tIrrigation_Technic_Factor_"<< i+1 << "(t):\t" << (*Irrigation_Technic_Factor[i])() << std::endl;
        }
    }

    void print_all_outputs() {
        for (int i=0; i < nbBele; i++) {
            std::cout << "\t\tRE_"<< i+1 << "(t):\t" << (*RE[i])() << std::endl;
            std::cout << "\t\tRS_"<< i+1 << "(t):\t" << (*RS[i])() << std::endl;
            std::cout << "\t\tRC_"<< i+1 << "(t):\t" << (*RC[i])() << std::endl;
            std::cout << "\t\tRT_"<< i+1 << "(t):\t" << (*RT[i])() << std::endl;
            std::cout << "\t\tR_"<< i+1 << "(t):\t" << (*R[i])() << std::endl;
            std::cout << "\t\tmIrrigationPriority_"<< i+1 << "(t):\t" << (*mIrrigationPriority[i])() << std::endl;
        }
    }

};

} // namespace ASCROM

DECLARE_DYNAMICS(ASCROM::IrrigationPriority)

