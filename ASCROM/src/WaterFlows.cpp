/**
  * @file WaterFlows.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace ASCROM {

using namespace vle::discrete_time;

class WaterFlows : public DiscreteTimeDyn
{
public:
WaterFlows (
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    Rain.init(this, "Rain", evts);
    getOptions().syncs.insert(std::make_pair("Rain", 1)); // force sync for in input
    ActualIrrigation.init(this, "ActualIrrigation", evts);
    getOptions().syncs.insert(std::make_pair("ActualIrrigation", 1)); // force sync for in input
    Max_evap.init(this, "Max_evap", evts);
    getOptions().syncs.insert(std::make_pair("Max_evap", 1)); // force sync for in input
    Max_transp.init(this, "Max_transp", evts);
    getOptions().syncs.insert(std::make_pair("Max_transp", 1)); // force sync for in input
    RootDepth.init(this, "RootDepth", evts);
    getOptions().syncs.insert(std::make_pair("RootDepth", 1)); // force sync for in input
    few.init(this, "few", evts);
    getOptions().syncs.insert(std::make_pair("few", 1)); // force sync for in input
    fc.init(this, "fc", evts);
    getOptions().syncs.insert(std::make_pair("fc", 1)); // force sync for in input
    kr1.init(this, "kr1", evts);
    getOptions().syncs.insert(std::make_pair("kr1", 1)); // force sync for in input
    kr2.init(this, "kr2", evts);
    getOptions().syncs.insert(std::make_pair("kr2", 1)); // force sync for in input

    Max_transp_R1.init(this, "Max_transp_R1", evts);
    Max_transp_R2.init(this, "Max_transp_R2", evts);
    Actual_evap.init(this, "Actual_evap", evts);
    Actual_transp_R1.init(this, "Actual_transp_R1", evts);
    Actual_transp_R2.init(this, "Actual_transp_R2", evts);
    Actual_transp.init(this, "Actual_transp", evts);
    RunOff.init(this, "RunOff", evts);
    I_R1.init(this, "I_R1", evts);
    I_R2.init(this, "I_R2", evts);
    Drain.init(this, "Drain", evts);
    WC_R1.init(this, "WC_R1", evts);
    WC_R2.init(this, "WC_R2", evts);
    WCtmp_R1.init(this, "WCtmp_R1", evts);
    WCtmp_R2.init(this, "WCtmp_R2", evts);
    stress_index.init(this, "stress_index", evts);
    DailyBalanceControl.init(this, "DailyBalanceControl", evts);

    CurveNumber = vv::toDouble(evts.get("CurveNumber"));
    Ia_SoilType_param = vv::toDouble(evts.get("Ia_SoilType_param"));
    TH1 = vv::toDouble(evts.get("TH1"));
    ThetaFC1 = vv::toDouble(evts.get("ThetaFC1"));
    ThetaWP1 = vv::toDouble(evts.get("ThetaWP1"));
    BD1 = vv::toDouble(evts.get("BD1"));
    TH2 = vv::toDouble(evts.get("TH2"));
    ThetaFC2 = vv::toDouble(evts.get("ThetaFC2"));
    ThetaWP2 = vv::toDouble(evts.get("ThetaWP2"));
    BD2 = vv::toDouble(evts.get("BD2"));
    
    AWC1 = (ThetaFC1 - ThetaWP1) * BD1 * TH1;
    AWC2 = (ThetaFC2 - ThetaWP2) * BD2 * TH2;
    S = 25400./CurveNumber - 254.;
    Ia = Ia_SoilType_param * S;

}

virtual ~WaterFlows ()
{}

void compute(const vle::devs::Time& /*t*/)
{
    
    if (Rain()>Ia) {
        RunOff = pow(Rain() - Ia, 2) / (Rain() - Ia + S);
    } else {
        RunOff = 0.;
    }
    
    I_R1 = std::max(0., Rain() + ActualIrrigation() - RunOff());
    
    I_R2 = std::max(0., WC_R1(-1) + I_R1() - AWC1);
    
    Drain = std::max(0., WC_R2(-1) + I_R2() - AWC2);
    
    WCtmp_R1 = WC_R1(-1) + I_R1() - I_R2();
    
    WCtmp_R2 = WC_R2(-1) + I_R2() - Drain();
    
    Max_transp_R1 = std::min(1., TH1 / (RootDepth()*1000.)) * Max_transp();
    
    Max_transp_R2 = std::max(0., (RootDepth()*1000. - TH1) / (RootDepth()*1000.)) * Max_transp();
    
     if ((Max_evap() + Max_transp_R1() <= WCtmp_R1())|(Max_evap()<few()*WCtmp_R1())) {
         Actual_evap = Max_evap();
     } else {
         Actual_evap = few() * WCtmp_R1();
     }

     if ( Max_evap() + Max_transp_R1() <= WCtmp_R1()) {
         Actual_transp_R1 = Max_transp_R1()*kr1();
     } else {
         Actual_transp_R1 = fc()*std::min(Max_transp_R1()*kr1() , WCtmp_R1());
     }
     
     Actual_transp_R2 = std::min(kr2()* (Max_transp_R1() - Actual_transp_R1() + Max_transp_R2()), WCtmp_R2()*std::max(0., (RootDepth()*1000.-TH1)/TH2));
     
     Actual_transp = Actual_transp_R1() + Actual_transp_R2();
     
     WC_R1 = std::min(AWC1, WCtmp_R1() - Actual_transp_R1() - Actual_evap());
     
     WC_R2 = std::min(AWC2, WCtmp_R2() - Actual_transp_R2());
     
     stress_index = (Max_transp() > 0.) ? Actual_transp()/Max_transp() : 1.;
     
     DailyBalanceControl = WC_R1()+WC_R2() - (WC_R1(-1)+WC_R2(-1)) - Rain() - ActualIrrigation() + RunOff() + Drain() + Actual_evap() + Actual_transp_R1() + Actual_transp_R2();

}

    Var Rain;
    Var ActualIrrigation;
    Var Max_evap;
    Var Max_transp;
    Var RootDepth;
    Var few;
    Var fc;
    Var kr1;
    Var kr2;

    Var Max_transp_R1;
    Var Max_transp_R2;
    Var Actual_evap;
    Var Actual_transp_R1;
    Var Actual_transp_R2;
    Var Actual_transp;
    Var RunOff;
    Var I_R1;
    Var I_R2;
    Var Drain;
    Var WC_R1;
    Var WC_R2;
    Var WCtmp_R1;
    Var WCtmp_R2;
    Var stress_index;
    Var DailyBalanceControl;

    double CurveNumber;
    double Ia_SoilType_param;
    double TH1;
    double ThetaFC1;
    double ThetaWP1;
    double BD1;
    double TH2;
    double ThetaFC2;
    double ThetaWP2;
    double BD2;
    
    double AWC1;
    double AWC2;
    double S;
    double Ia;
};

} // namespace ASCROM

DECLARE_DYNAMICS(ASCROM::WaterFlows )

