/**
  * @file IrrigationDemand.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <vle/utils/Tools.hpp>

namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace ASCROM {

using namespace vle::discrete_time;

class IrrigationDemand : public DiscreteTimeDyn
{
public:
IrrigationDemand (
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    std::string ModelName = getModel().getCompleteName();
    // daily inputs
    Rain7days.init(this, "Rain7days", evts);
    getOptions().syncs.insert(std::make_pair("Rain7days", 1)); // force sync for in input
    Irrigation7days.init(this, "Irrigation7days", evts);
    Is_Failure.init(this, "Is_Failure", evts);
    CropWaterStress.init(this, "CropWaterStress", evts);
    SoilWaterContent.init(this, "SoilWaterContent", evts);
    
    IrrigationDose_interp.init(this, "IrrigationDose_interp", evts);
    getOptions().syncs.insert(std::make_pair("IrrigationDose_interp", 1)); // force sync for in input
    Rain_Threshold_interp.init(this, "Rain_Threshold_interp", evts);
    getOptions().syncs.insert(std::make_pair("Rain_Threshold_interp", 1)); // force sync for in input
    SoilWaterContent_Threshold_interp.init(this, "SoilWaterContent_Threshold_interp", evts);
    getOptions().syncs.insert(std::make_pair("SoilWaterContent_Threshold_interp", 1)); // force sync for in input
    CropWaterStress_Threshold_interp.init(this, "CropWaterStress_Threshold_interp", evts);
    getOptions().syncs.insert(std::make_pair("CropWaterStress_Threshold_interp", 1)); // force sync for in input

    // state variables
    Irrigation_Demand.init(this, "IrrigationDemand", evts);
    IrrigationDose.init(this, "IrrigationDose", evts);
    Rain_Threshold.init(this, "Rain_Threshold", evts);
    CropWaterStress_Threshold.init(this, "CropWaterStress_Threshold", evts);
    SoilWaterContent_Threshold.init(this, "SoilWaterContent_Threshold", evts);
    
    // parameters
    Rule_Strat = vv::toString(evts.get("Rule_Strat"));

    if ((Rule_Strat!="None") && (Rule_Strat!="Rain_Threshold") && (Rule_Strat!="SoilWaterContent_Threshold") && (Rule_Strat!="CropWaterStress_Threshold")) {
        throw vu::ModellingError(
                vu::format("[%s] Rule_Strat parameter error : option not recognized %s \n(Valid options are: None, Rain_Threshold, SoilWaterContent_Threshold, CropWaterStress_Threshold)",
                        ModelName.c_str(), Rule_Strat.c_str()));
    }
    
}

virtual ~IrrigationDemand ()
{}

void compute(const vle::devs::Time& /*t*/)
{
    IrrigationDose = Is_Failure(-1) ? 0 : IrrigationDose_interp();

    if (IrrigationDose()>0.0) {
        bool start_rule = false;
        if (Rule_Strat=="None") {
            start_rule=true;
        } else if (Rule_Strat=="Rain_Threshold") {
            Rain_Threshold = Is_Failure(-1) ? 0 : Rain_Threshold_interp();
            start_rule=(Rain7days()+Irrigation7days(-1)<Rain_Threshold());
        } else if (Rule_Strat=="SoilWaterContent_Threshold") {
            SoilWaterContent_Threshold = Is_Failure(-1) ? 0 : SoilWaterContent_Threshold_interp();
            start_rule=(SoilWaterContent(-1)<SoilWaterContent_Threshold());
        } else if (Rule_Strat=="CropWaterStress_Threshold") {
            CropWaterStress_Threshold = Is_Failure(-1) ? 0 : CropWaterStress_Threshold_interp();
            start_rule=(CropWaterStress(-1)<CropWaterStress_Threshold());
        } // test sur Rule_Strat dans le constructeur nous permet de ne pas considerer le dernier cas (else)

        if (start_rule) {
            Irrigation_Demand = IrrigationDose();
        } else {
            Irrigation_Demand = 0.;
        }
    } else {
        Irrigation_Demand = 0.;
    }
}
    // daily inputs
    Var Rain7days;
    Var Irrigation7days;
    Var Is_Failure;
    Var CropWaterStress;
    Var SoilWaterContent;
    Var IrrigationDose_interp;
    Var Rain_Threshold_interp;
    Var SoilWaterContent_Threshold_interp;
    Var CropWaterStress_Threshold_interp;
    // state variables
    Var Irrigation_Demand;
    Var IrrigationDose; //the reference Irrigation amount to use (changes with crop)
    Var Rain_Threshold;
    Var CropWaterStress_Threshold;
    Var SoilWaterContent_Threshold;
    //parameters
    std::string Rule_Strat;
    // local variables

};

} // namespace ASCROM

DECLARE_DYNAMICS(ASCROM::IrrigationDemand)

