/**
  * @file ClimaticWaterDemand.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <vle/utils/Tools.hpp>


namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace ASCROM {

using namespace vle::discrete_time;

class ClimaticWaterDemand : public DiscreteTimeDyn
{
public:
ClimaticWaterDemand (
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    ET0.init(this, "ET0", evts);
    getOptions().syncs.insert(std::make_pair("ET0", 1)); // force sync for in input
    RHmin.init(this, "RHmin", evts);
    getOptions().syncs.insert(std::make_pair("RHmin", 1)); // force sync for in input
    u2.init(this, "u2", evts);
    getOptions().syncs.insert(std::make_pair("u2", 1)); // force sync for in input
    Rain.init(this, "Rain", evts);
    getOptions().syncs.insert(std::make_pair("Rain", 1)); // force sync for in input
    Is_Day_Without_Irrigation.init(this, "Is_Day_Without_Irrigation", evts);
    getOptions().syncs.insert(std::make_pair("Is_Day_Without_Irrigation", 1)); // force sync for in input
    YieldFraction.init(this, "YieldFraction", evts);
    WC_R1.init(this, "WC_R1", evts);
    WC_R2.init(this, "WC_R2", evts);

    kcb_interp.init(this, "kcb_interp", evts);
    getOptions().syncs.insert(std::make_pair("kcb_interp", 1)); // force sync for in input
    kcini_interp.init(this, "kcini_interp", evts);
    getOptions().syncs.insert(std::make_pair("kcini_interp", 1)); // force sync for in input
    kcmid_interp.init(this, "kcmid_interp", evts);
    getOptions().syncs.insert(std::make_pair("kcmid_interp", 1)); // force sync for in input
    h_interp.init(this, "h_interp", evts);
    getOptions().syncs.insert(std::make_pair("h_interp", 1)); // force sync for in input
    MaxDepthRoots_interp.init(this, "MaxDepthRoots_interp", evts);
    getOptions().syncs.insert(std::make_pair("MaxDepthRoots_interp", 1)); // force sync for in input
    Rain_Threshold_To_Reset_Days_Without_Water.init(this, "Rain_Threshold_To_Reset_Days_Without_Water", evts);
    getOptions().syncs.insert(std::make_pair("Rain_Threshold_To_Reset_Days_Without_Water", 1)); // force sync for in input
    Lethal_Threshold_Days_Without_Water.init(this, "Lethal_Threshold_Days_Without_Water", evts);
    getOptions().syncs.insert(std::make_pair("Lethal_Threshold_Days_Without_Water", 1)); // force sync for in input
    Lethal_Threshold_YieldFraction.init(this, "Lethal_Threshold_YieldFraction", evts);
    getOptions().syncs.insert(std::make_pair("Lethal_Threshold_YieldFraction", 1)); // force sync for in input
    NoFailurePeriod.init(this, "NoFailurePeriod", evts);
    getOptions().syncs.insert(std::make_pair("NoFailurePeriod", 1)); // force sync for in input

    Days_Since_Sowing.init(this, "Days_Since_Sowing", evts);
    Days_Without_Water.init(this, "Days_Without_Water", evts);
    Is_Failure.init(this, "Is_Failure", evts);
    Failure_counter.init(this, "Failure_counter", evts);
    Is_Failure_From_WaterSupply.init(this, "Is_Failure_From_WaterSupply", evts);
    Is_Failure_From_YieldFraction.init(this, "Is_Failure_From_YieldFraction", evts);
    kcb.init(this, "kcb", evts);
    h.init(this, "h", evts);
    MaxDepthRoots.init(this, "MaxDepthRoots", evts);
    kcini.init(this, "kcini", evts);
    kcmid.init(this, "kcmid", evts);
    ke.init(this, "ke", evts);
    RootDepth.init(this, "RootDepth", evts);
    Max_transp.init(this, "Max_transp", evts);
    Max_evap.init(this, "Max_evap", evts);
    PET.init(this, "PET", evts);
    kcmax.init(this, "kcmax", evts);
    kr1.init(this, "kr1", evts);
    kr2.init(this, "kr2", evts);
    fc.init(this, "fc", evts);
    few.init(this, "few", evts);
    
    ModelName = getModel().getCompleteName();
    SoilDepth = vv::toDouble(evts.get("SoilDepth"));
    REW1 = vv::toDouble(evts.get("REW1"));
    REW2 = vv::toDouble(evts.get("REW2"));
    fw_Rain_Threshold = vv::toDouble(evts.get("fw_Rain_Threshold"));
    fw = vv::toDouble(evts.get("fw"));
    Failure_Option = vv::toInteger(evts.get("Failure_Option"));
    few_parameter = (evts.exist("few_parameter")) ? vv::toDouble(evts.get("few_parameter")) : 1.0;
    isDrip = (evts.exist("isDrip")) ? vv::toBoolean(evts.get("isDrip")) : false;
}

virtual ~ClimaticWaterDemand ()
{}

void compute(const vle::devs::Time& /*t*/)
{

    bool isCrop = (kcb_interp()>0);
    
    Is_Failure_From_WaterSupply = isCrop 
                                  & ((bool)Is_Failure_From_WaterSupply(-1) 
                                     | (Days_Without_Water(-1) >= Lethal_Threshold_Days_Without_Water()));
    Is_Failure_From_YieldFraction = isCrop 
                                    & ((bool)Is_Failure_From_YieldFraction(-1) 
                                       | ((Days_Since_Sowing(-1) > NoFailurePeriod()) 
                                          & (YieldFraction(-1) <= Lethal_Threshold_YieldFraction())));
    
    if (Failure_Option==1){
        Is_Failure = Is_Failure_From_WaterSupply();
    } else if (Failure_Option==2) {
        Is_Failure = Is_Failure_From_YieldFraction();
    } else if (Failure_Option==3) {
        Is_Failure = ((bool)Is_Failure_From_WaterSupply() & (bool)Is_Failure_From_YieldFraction());
    } else if (Failure_Option==4) {
        Is_Failure = ((bool)Is_Failure_From_WaterSupply() | (bool)Is_Failure_From_YieldFraction());
    } else {
        Is_Failure = 0;
    }

    bool newFailure = (Is_Failure(-1)==0) & (Is_Failure()==1);

    if (newFailure) {
        Failure_counter = Failure_counter(-1) + 1;
    } else {
        Failure_counter = Failure_counter(-1);
    }

    
    bool Is_Day_Without_Water = isCrop & ((Rain()<Rain_Threshold_To_Reset_Days_Without_Water()) & ((bool)Is_Day_Without_Irrigation()));
    
    if (isCrop & (bool)Is_Failure()) {
        Days_Without_Water = Days_Without_Water(-1);
    } else if (isCrop & Is_Day_Without_Water) {
        Days_Without_Water = Days_Without_Water(-1) + 1;
    } else {
        Days_Without_Water = 0;
    }

    kcb = Is_Failure() ? 0 : kcb_interp();
    h = Is_Failure() ? 0 : h_interp();
    MaxDepthRoots = Is_Failure() ? 0 :  MaxDepthRoots_interp();
    kcini = Is_Failure() ? 0 : kcini_interp();
    kcmid = Is_Failure() ? 0 : kcmid_interp();

    if (kcb()>0.) {
        Days_Since_Sowing = Days_Since_Sowing(-1) + 1.;
    } else {
        Days_Since_Sowing=0;
    }

    double climate_correction = (0.04 * (u2() - 2.) - 0.004 * (RHmin() - 45.)) * std::pow(h() / 3., 0.3);
    kcmax = std::max(kcb() + 0.05, 1.2 + climate_correction);

    kr1 = std::min(1., WC_R1(-1)/(REW1));

    kr2 = std::min(1., WC_R2(-1)/(REW2));

    fc = (kcb()>0.) ? std::pow((kcb() - kcini())/(kcmax() - kcini()), 1+0.5*h()) : 0.;

    double fw_Rain;
    if (Rain()<fw_Rain_Threshold) {
        fw_Rain = fw;
    } else {
        fw_Rain = 1.;
    }
    few = isDrip ? std::max(0., 1. - fc() * 2. / 3.) * fw : std::min(fw_Rain, std::max(0., 1. - few_parameter * fc()));

    ke = std::min(kr1() * (kcmax() - kcb()), few() * kcmax());

    RootDepth = std::min(MaxDepthRoots(), std::min(SoilDepth, MaxDepthRoots() * kcb()/kcmid()));

    Max_transp = kcb() * ET0();

    Max_evap = ke() * ET0();

    PET = Max_evap() + Max_transp();
}

    Var ET0;
    Var WC_R1;
    Var WC_R2;
    Var RHmin;
    Var u2;
    Var Rain;
    Var Is_Day_Without_Irrigation;
    Var YieldFraction;
    Var kcb_interp;
    Var kcini_interp;
    Var kcmid_interp;
    Var h_interp;
    Var MaxDepthRoots_interp;
    Var Rain_Threshold_To_Reset_Days_Without_Water;
    Var Lethal_Threshold_Days_Without_Water;
    Var Lethal_Threshold_YieldFraction;

    Var Days_Since_Sowing;
    Var Days_Without_Water;
    Var Is_Failure;
    Var Failure_counter;
    Var Is_Failure_From_WaterSupply;
    Var Is_Failure_From_YieldFraction;
    Var NoFailurePeriod;
    Var kcb;
    Var h;
    Var MaxDepthRoots;
    Var kcini;
    Var kcmid;
    Var ke;
    Var RootDepth;
    Var Max_transp;
    Var Max_evap;
    Var PET;
    Var kcmax;
    Var kr1;
    Var kr2;
    Var fc;
    Var few;

    double RD_Coeff;
    double MaxDepthRoots_t0;
    std::map<int, std::map<std::string, double>> MaxDepthRoots_pairs;
    double SoilDepth;
    double REW1;
    double REW2;
    double fw;
    double fw_Rain_Threshold;
    int Failure_Option;
    double few_parameter;
    bool isDrip;
    
    std::string ModelName;

};

} // namespace ASCROM

DECLARE_DYNAMICS(ASCROM::ClimaticWaterDemand )

