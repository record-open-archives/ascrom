/**
  * @file ActualIrrigation.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace ASCROM {

using namespace vle::discrete_time;

class ActualIrrigation : public DiscreteTimeDyn
{
public:
ActualIrrigation (
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    Irrigation.init(this, "Irrigation", evts);
    getOptions().syncs.insert(std::make_pair("Irrigation", 1)); // force sync for in input

    mActualIrrigation.init(this, "ActualIrrigation", evts);

    Irrig_Efficiency = vv::toDouble(evts.get("Irrig_Efficiency"));
}

virtual ~ActualIrrigation ()
{}

void compute(const vle::devs::Time& /*t*/)
{
    double mIrrig_Efficiency = std::max(0.01, Irrig_Efficiency);
    mActualIrrigation =  Irrigation() * mIrrig_Efficiency;
}

    Var Irrigation;

    Var mActualIrrigation;

    double Irrig_Efficiency;

};

} // namespace ASCROM

DECLARE_DYNAMICS(ASCROM::ActualIrrigation)

