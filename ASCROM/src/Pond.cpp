/**
  * @file Pond.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <iostream>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace ASCROM {

using namespace vle::discrete_time;

class Pond : public DiscreteTimeDyn
{
public:
Pond (
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    ET0.init(this, "ET0", evts);
    getOptions().syncs.insert(std::make_pair("ET0", 1)); // force sync for in input
    Rain.init(this, "Rain", evts);
    getOptions().syncs.insert(std::make_pair("Rain", 1)); // force sync for in input
    Pond_Irrigation.init(this, "Pond_Irrigation", evts);
    getOptions().syncs.insert(std::make_pair("Pond_Irrigation", 1)); // force sync for in input
    AvailableWater.init(this, "AvailableWater", evts);
    getOptions().syncs.insert(std::make_pair("AvailableWater", 1)); // force sync for in input
    RunOff.init(this, "RunOff", evts);
    getOptions().syncs.insert(std::make_pair("RunOff", 1)); // force sync for in input


    Evaporation.init(this, "Evaporation", evts);
    Percolation.init(this, "Percolation", evts);
    Pump_water_refill.init(this, "Pump_water_refill", evts);
    Pond_Overflow.init(this, "Pond_Overflow", evts);
    X.init(this, "X", evts);
    Y.init(this, "Y", evts);
    Z.init(this, "Z", evts);
    S.init(this, "S", evts);
    V.init(this, "V", evts);

    DbgLog = (evts.exist("DbgLog")) ? vv::toDouble(evts.get("DbgLog")) : 0.;
    if (DbgLog > 0.) {
        std::cout << "[Pond] (" << getModel().getCompleteName() << ") Initialization steps ..." <<std::endl;
    }
    X0 =  vv::toDouble(evts.get("X0"));
    Y0 =  vv::toDouble(evts.get("Y0"));
    Z0 =  vv::toDouble(evts.get("Z0"));
    fx =  vv::toDouble(evts.get("fx"));
    Evap_coeff =  vv::toDouble(evts.get("Evap_coeff"));
    Percol_coeff =  vv::toDouble(evts.get("Percol_coeff"));
    Init_Pond_Frac =  vv::toDouble(evts.get("Init_Pond_Frac"));

    fy = Y0 * fx / X0;
    S0 = X0 * Y0;
    X1 = X0 - fx * Z0;
    Y1 = Y0 - fy * Z0;
    Z1 = X0 / fx;
    S1 = X1 * Y1;
    V0 = Z0 * (S0 + S1 + std::sqrt(S0 * S1)) / 3.;
    V1 = std::pow(Z1 - Z0, 3) * fx * fy / 3.;
    
    if (DbgLog > 0.) {
        std::cout << "\tUsing Parameters :" <<std::endl;
        print_all_parameters();
    }
    
    double V_init = V0 * Init_Pond_Frac;
    V.init_value(V_init);
    double Z_init = get_Z(V_init, Z0, Z1, V1, fx, fy);
    Z.init_value(Z_init);
    X.init_value(get_XY(Z_init + Z1 - Z0, fx));
    Y.init_value(get_XY(Z_init + Z1 - Z0, fy));

    if (DbgLog > 0.) {
        std::cout << "State Var Init:" << std::endl;
        std::cout << "\t\tV:\t" << V_init << std::endl;
        std::cout << "\t\tZ:\t" << Z_init << std::endl;
        std::cout << "\t\tX:\t" << get_XY(Z_init + Z1 - Z0, fx) << std::endl;
        std::cout << "\t\tY:\t" << get_XY(Z_init + Z1 - Z0, fy) << std::endl;
    }
}

virtual ~Pond ()
{}

void compute(const vle::devs::Time& t)
{
    if (DbgLog > 0.) {
        std::cout << t << "\t[Pond] (" << getModel().getCompleteName() << ") Starting daily computations..." <<std::endl;
    }
    if (DbgLog > 1.) {
        std::cout << "\tDaily inputs :" <<std::endl;
        print_all_inputs();
    }

    if (Pond_Irrigation() > V(-1)) {
        std::cout << "Warning!!!Irrigation request is larger than Pond volume!!!\t" << Pond_Irrigation() << "\t>\t" << V(-1) << "\tMay lead to negative Percolation..."<<std::endl;
    }

    Percolation = std::min(Z(-1) * Percol_coeff * S1, V(-1) + Rain() * S0 / 1000. + RunOff() - Pond_Irrigation());

    Evaporation = std::min(ET0() * Evap_coeff * S(-1) / 1000., V(-1) - Percolation() + Rain() * S0 / 1000. + RunOff() - Pond_Irrigation());

    const double net_flow = Rain() * S0 / 1000. + RunOff() - Pond_Irrigation() - Evaporation() - Percolation();
    Pump_water_refill = std::min(AvailableWater(), V0 - std::min(V0, (V(-1) + net_flow)));

    Pond_Overflow = std::max(0., V(-1) + net_flow + AvailableWater() - V0);

    V = V(-1) + net_flow + AvailableWater() - Pond_Overflow();

    Z = get_Z(V(), Z0, Z1, V1, fx, fy);

    X = get_XY(Z() + Z1 - Z0, fx);

    Y = get_XY(Z() + Z1 - Z0, fy);

    S = X() * Y();

    // print all daily state variables outputs
    if (DbgLog > 1.) {
        std::cout << "\tDaily state variables :" <<std::endl;
        print_all_outputs();
    }

}

    Var ET0;
    Var Rain;
    Var RunOff;
    Var Pond_Irrigation;
    Var AvailableWater;
    
    Var Evaporation;
    Var Percolation;
    Var Pump_water_refill;
    Var Pond_Overflow;
    Var V;
    Var X;
    Var Y;
    Var S;
    Var Z;

    double DbgLog;
    double X0, Y0, Z0, S0, V0;
    double X1, Y1, Z1, S1, V1;
    double fx, fy;
    double Evap_coeff;
    double Percol_coeff;
    double Init_Pond_Frac;

    double get_Z(const double V, const double Z0, const double Z1, const double V1, const double fx, const double fy) {
        return Z0 - Z1 + std::cbrt(3 * (V + V1) / (fx * fy));
    }

    double get_XY(const double Z, const double fxy) {
        return Z * fxy;
    }

    void print_all_parameters() {
        std::cout << "\t\tDbgLog:\t" << DbgLog << std::endl;
        std::cout << "\t\tfx:\t" << fx << std::endl;
        std::cout << "\t\tX0:\t" << X0 << std::endl;
        std::cout << "\t\tY0:\t" << Y0 << std::endl;
        std::cout << "\t\tZ0:\t" << Z0 << std::endl;
        std::cout << "\t\tEvap_coeff:\t" << Evap_coeff << std::endl;
        std::cout << "\t\tPercol_coeff:\t" << Percol_coeff << std::endl;
        std::cout << "\t\tInit_Pond_Frac:\t" << Init_Pond_Frac << std::endl;
        
        std::cout << "Local Parameters Estimated:" << std::endl;
        std::cout << "\t\tfy:\t" << fy << std::endl;
        std::cout << "\t\tS0:\t" << S0 << std::endl;
        std::cout << "\t\tX1:\t" << X1 << std::endl;
        std::cout << "\t\tY1:\t" << Y1 << std::endl;
        std::cout << "\t\tZ1:\t" << Z1 << std::endl;
        std::cout << "\t\tS1:\t" << S1 << std::endl;
        std::cout << "\t\tV0:\t" << V0 << std::endl;
        std::cout << "\t\tV1:\t" << V1 << std::endl;
    } 

    void print_all_inputs() {
        std::cout << "\t\tET0(t):\t" << ET0() << "\t(mm)" << std::endl;
        std::cout << "\t\tRain(t):\t" << Rain() << "\t(mm)" << std::endl;
        std::cout << "\t\tRunOff(t):\t" << RunOff() << "\t(m3)" << std::endl;
        std::cout << "\t\tPond_Irrigation(t):\t" << Pond_Irrigation() << "\t(m3)" << std::endl;
        std::cout << "\t\tAvailableWater(t):\t" << AvailableWater() << "\t(m3)" << std::endl;
    }

    void print_all_outputs() {
        std::cout << "\t\tEvaporation(t):\t" << Evaporation()  << "\t(m3)"<< std::endl;
        std::cout << "\t\tPercolation(t):\t" << Percolation()  << "\t(m3)"<< std::endl;
        std::cout << "\t\tPump_water_refill(t):\t" << Pump_water_refill()  << "\t(m3)"<< std::endl;
        std::cout << "\t\tPond_Overflow(t):\t" << Pond_Overflow()  << "\t(m3)"<< std::endl;
        std::cout << "\t\tV(t):\t" << V()  << "\t(m3)"<< std::endl;
        std::cout << "\t\tX(t):\t" << X()  << "\t(m)"<< std::endl;
        std::cout << "\t\tY(t):\t" << Y()  << "\t(m)"<< std::endl;
        std::cout << "\t\tZ(t):\t" << Z()  << "\t(m)"<< std::endl;
        std::cout << "\t\tS(t):\t" << S()  << "\t(m2)"<< std::endl;

    }


};

} // namespace ASCROM

DECLARE_DYNAMICS(ASCROM::Pond)

