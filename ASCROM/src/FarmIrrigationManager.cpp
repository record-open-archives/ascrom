/**
  * @file FarmIrrigationManager.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <vle/utils/Tools.hpp> // vle::utils::ModellingError
#include <numeric> // std::accumulate
#include <algorithm> // std::sort

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

namespace ASCROM {

using namespace vle::discrete_time;

class FarmIrrigationManager : public DiscreteTimeDyn
{
public:
FarmIrrigationManager (
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    // parameters
    DbgLog = (evts.exist("DbgLog")) ? vv::toDouble(evts.get("DbgLog")) : 0.;
    if (DbgLog > 0.) {
        std::cout << "[FarmIrrigationManager] (" << getModel().getCompleteName() << ") Initialization steps ..." <<std::endl;
    }

    nbBele = vv::toInteger(evts.get("nbBele"));
    isPondForIrrigation = vv::toBoolean(evts.get("isPondForIrrigation"));
    isOptimize = vv::toBoolean(evts.get("isOptimize"));
    isRefill = vv::toBoolean(evts.get("isRefill"));
    V0 = vv::toDouble(evts.get("V0"));
    for (int i=0; i < nbBele; i++) {
        std::string plotarea_paramname = "PlotArea_"+std::to_string(i+1);
        PlotAreas.push_back(vv::toDouble(evts.get(plotarea_paramname)));
    }
    if (DbgLog > 0.) {
        std::cout << "\tUsing Parameters :" <<std::endl;
        print_all_parameters();
    }

    if (DbgLog > 0.) {
        std::cout << "\tDeclaring Input/Output variables..." <<std::endl;
    }
    // daily inputs
    V.init(this, "V", evts);
    AvailableWater.init(this, "AvailableWater", evts);
    getOptions().syncs.insert(std::make_pair("AvailableWater", 1)); // force sync for in input
    PumpMaxVol.init(this, "PumpMaxVol", evts);
    getOptions().syncs.insert(std::make_pair("PumpMaxVol", 1)); // force sync for in input
    // state variables
    PondExtraction.init(this, "PondExtraction", evts);
    PumpExtraction.init(this, "PumpExtraction", evts);
    PondRefill.init(this, "PondRefill", evts);
    WaterCycle_Duration.init(this, "WaterCycle_Duration", evts);
    WaterCycle_RemainingAmount.init(this, "WaterCycle_RemainingAmount", evts);
    WaterCycle_CurrentPlot.init(this, "WaterCycle_CurrentPlot", evts);
    WaterCycle_CurrentDose.init(this, "WaterCycle_CurrentDose", evts);
    
    for (int i=0; i < nbBele; i++) {
        // daily inputs
        Irrigation_Demands.push_back(generate_Var(evts, "Irrigation_Demand", i, true, DbgLog));
        Max_Irrigation_Durations.push_back(generate_Var(evts, "Max_Irrigation_Duration", i, true, DbgLog));
        IrrigationPriorities.push_back(generate_Var(evts, "IrrigationPriority", i, true, DbgLog));

        // state variables
        IrrigationAmounts.push_back(generate_Var(evts, "IrrigationAmount", i, false, DbgLog));
        Irrigations.push_back(generate_Var(evts, "Irrigation", i, false, DbgLog));
        Is_Day_Without_Irrigation.push_back(generate_Var(evts, "Is_Day_Without_Irrigation", i, false, DbgLog));
        Days_Without_Irrigation.push_back(generate_Var(evts, "Days_Without_Irrigation", i, false, DbgLog));
        PondExtractions.push_back(generate_Var(evts, "PondExtraction", i, false, DbgLog));
        PumpExtractions.push_back(generate_Var(evts, "PumpExtraction", i, false, DbgLog));
    }

}

virtual ~FarmIrrigationManager ()
{}

void compute(const vle::devs::Time& t)
{
    if (DbgLog > 0.) {
        std::cout << t << "\t[FarmIrrigationManager] (" << getModel().getCompleteName() << ") Starting daily computations..." <<std::endl;
    }
    if (DbgLog > 1.) {
        std::cout << "\tDaily inputs :" <<std::endl;
        print_all_inputs();
    }

    {
        if (DbgLog > 1.) std::cout << "\tChecking IrrigationPriority inputs unicity..." << std::endl;
        check_priorities_unicity(get_VarVector(IrrigationPriorities), DbgLog);
    }

    // local variables for iterative update
    std::vector<double> pond_extractions;
    std::vector<double> pump_extractions;
    double pond_AvailableIrrigationWater = (isPondForIrrigation ? V(-1) : 0.);
    double pump_AvailableIrrigationWater = AvailableWater();
    double total_AvailableIrrigationWater = pond_AvailableIrrigationWater + pump_AvailableIrrigationWater;
    std::vector<double> irrigation_amounts;
    std::vector<double> is_day_without_irrigation;
    for (int i=0; i < nbBele; i++) {
        irrigation_amounts.push_back(0.);
        is_day_without_irrigation.push_back(1.);
        pond_extractions.push_back(0.);
        pump_extractions.push_back(0.);
    }


    double total_Irrigation_Demand = get_total_Irrigation_Demand(get_VarVector(Irrigation_Demands), PlotAreas); // total amount in m3

    bool is_WaterCycle_Active = (WaterCycle_Duration(-1) > 0.);
    if (is_WaterCycle_Active) {
        if (DbgLog > 0.) {
            std::cout << "\tActive Water Cycle duration: " << WaterCycle_Duration(-1) << "\ton plot " << WaterCycle_CurrentPlot(-1)+1 << "\twith dose "<< WaterCycle_CurrentDose() << " (m3)\tRemainder: " << WaterCycle_RemainingAmount(-1)<< " (m3)"  <<std::endl;
        }

        WaterCycle_Duration = (int)WaterCycle_Duration(-1) - 1;
        pump_extractions[WaterCycle_CurrentPlot(-1)] = std::min(WaterCycle_CurrentDose(), WaterCycle_RemainingAmount(-1));

        WaterCycle_RemainingAmount = std::max(0., WaterCycle_RemainingAmount(-1) - WaterCycle_CurrentDose());
        if (WaterCycle_Duration() == 0.) {
            WaterCycle_CurrentPlot = -1;
            WaterCycle_CurrentDose = 0.;
            if (DbgLog > 0.) {
                std::cout << "\tEnd of Water Cycle." <<std::endl;
            }
            
            if (isOptimize) {
                if (DbgLog > 0.) std::cout << "\tAttempt to optimize remaining water." <<std::endl;
                bool is_IrrigationDay = (total_Irrigation_Demand > 0.);
                if (is_IrrigationDay) {
                    pump_AvailableIrrigationWater = WaterCycle_CurrentDose(-1) - WaterCycle_RemainingAmount(-1);
                    total_AvailableIrrigationWater = pond_AvailableIrrigationWater + pump_AvailableIrrigationWater;
                    if (DbgLog > 0.) {
                        std::cout << "\tDaily total water demand: " << total_Irrigation_Demand <<" (m3)" << std::endl;
                        if (DbgLog > 1.) {
                            std::cout <<"\t\t[Plot 1: " << (*Irrigation_Demands[0])() << " (mm) with area: " << PlotAreas[0] << " (m2) ]\t-> "<< convert_mm_to_m3((*Irrigation_Demands[0])(), PlotAreas[0]) << " (m3);" << std::endl;
                            for (int i=1; i < nbBele; i++) {
                                std::cout << "\t\t[Plot " << i+1 << ": " << (*Irrigation_Demands[i])() << " (mm) with area: " << PlotAreas[i] << " (m2) ]\t-> "<< convert_mm_to_m3((*Irrigation_Demands[i])(), PlotAreas[i]) << " (m3);" << std::endl;
                            }
                        }
                        std::cout << "\tAvailable supply is: " << total_AvailableIrrigationWater << " (m3) =\tfrom pond: " << pond_AvailableIrrigationWater << " (m3) +\tfrom pump: " << pump_AvailableIrrigationWater << " (m3)" << std::endl;
                    }

                    std::vector<int> indexplots = get_ranking_index(get_VarVector(IrrigationPriorities));
                    if (DbgLog > 1.) {
                        std::vector<double> croppriorities = get_VarVector(IrrigationPriorities);
                        for (unsigned int i = 0 ; i != indexplots.size() ; i++) {
                            std::cout <<"\t\tPlot : " << indexplots[i]+1<< " with priority "<< croppriorities[indexplots[i]] << "\t-> rank index "<< i << std::endl;
                        }
                    }

                    for (unsigned int i = 0 ; i != indexplots.size() ; i++) {// loop over plots in ranking order
                        if ((*Irrigation_Demands[indexplots[i]])() > 0.) {
                           double plot_irrig_demand = convert_mm_to_m3((*Irrigation_Demands[indexplots[i]])(), PlotAreas[indexplots[i]]);
                           if (DbgLog > 0.) {
                               std::cout <<"\t\tPlot : " << indexplots[i]+1 << "\twater demand is: " << plot_irrig_demand << " (m3)\tdaily water supply: " << total_AvailableIrrigationWater << " (m3)" << std::endl;
                           }
                           
                           if ( plot_irrig_demand <= total_AvailableIrrigationWater ) {
                               double pond_flow = std::min(pond_AvailableIrrigationWater, plot_irrig_demand);
                               double pump_flow = plot_irrig_demand - std::min(pond_AvailableIrrigationWater, plot_irrig_demand);
                               
                               pond_AvailableIrrigationWater -= pond_flow;
                               pump_AvailableIrrigationWater -= pump_flow;
                               
                               pond_extractions[indexplots[i]] += pond_flow;
                               pump_extractions[indexplots[i]] += pump_flow;
                               
                               total_AvailableIrrigationWater = pond_AvailableIrrigationWater + pump_AvailableIrrigationWater;
                               
                               irrigation_amounts[indexplots[i]] += plot_irrig_demand;
                               is_day_without_irrigation[indexplots[i]] = 0.;
                               
                               if (DbgLog > 0.) std::cout << "\t\t\tAll water demand provided in a single day: " << irrigation_amounts[indexplots[i]] << " (m3)" << std::endl;
                               if (DbgLog > 1.) std::cout << "\t\t\t\tRemaining available supply is: " << total_AvailableIrrigationWater << " (m3) =\tfrom pond: " << pond_AvailableIrrigationWater << " (m3) +\tfrom pump: " << pump_AvailableIrrigationWater << " (m3)" << std::endl;
                           }
                       }
                   }
                }
            }
        }
    } else {
        if (DbgLog > 0.) {
            std::cout << "\tNo active Water Cycle." <<std::endl;
        }
        bool is_IrrigationDay = (total_Irrigation_Demand > 0.);
        if (is_IrrigationDay) {
            double cycle_max_amount = PumpMaxVol() + pond_AvailableIrrigationWater;
            if (DbgLog > 0.) {
                std::cout << "\tDaily total water demand: " << total_Irrigation_Demand <<" (m3)" << std::endl;
                if (DbgLog > 1.) {
                    std::cout <<"\t\t[Plot 1: " << (*Irrigation_Demands[0])() << " (mm) with area: " << PlotAreas[0] << " (m2) ]\t-> "<< convert_mm_to_m3((*Irrigation_Demands[0])(), PlotAreas[0]) << " (m3);" << std::endl;
                    for (int i=1; i < nbBele; i++) {
                        std::cout << "\t\t[Plot " << i+1 << ": " << (*Irrigation_Demands[i])() << " (mm) with area: " << PlotAreas[i] << " (m2) ]\t-> "<< convert_mm_to_m3((*Irrigation_Demands[i])(), PlotAreas[i]) << " (m3);" << std::endl;
                    }
                }
                std::cout << "\tAvailable supply is: " << total_AvailableIrrigationWater << " (m3) =\tfrom pond: " << pond_AvailableIrrigationWater << " (m3) +\tfrom pump: " << pump_AvailableIrrigationWater << " (m3) \tmax available with cycle: " << cycle_max_amount << " (m3)" << std::endl;
            }

            std::vector<int> indexplots = get_ranking_index(get_VarVector(IrrigationPriorities));
            if (DbgLog > 1.) {
                std::vector<double> croppriorities = get_VarVector(IrrigationPriorities);
                for (unsigned int i = 0 ; i != indexplots.size() ; i++) {
                    std::cout <<"\t\tPlot : " << indexplots[i]+1<< " with priority "<< croppriorities[indexplots[i]] << "\t-> rank index "<< i << std::endl;
                }
            }

            for (unsigned int i = 0 ; i != indexplots.size() ; i++) {// loop over plots in ranking order
                if ((*Irrigation_Demands[indexplots[i]])() > 0.) {
                   double plot_irrig_demand = convert_mm_to_m3((*Irrigation_Demands[indexplots[i]])(), PlotAreas[indexplots[i]]);
                   double plot_cycle_duration = ceil((plot_irrig_demand-total_AvailableIrrigationWater)/AvailableWater());
                   if (DbgLog > 0.) {
                       std::cout <<"\t\tPlot : " << indexplots[i]+1 << "\twater demand is: " << plot_irrig_demand << " (m3)\tdaily water supply: " << total_AvailableIrrigationWater << " (m3) \tmax available with cycle: " << cycle_max_amount << " (m3)" << std::endl;
                   }
                   
                   if ( ( WaterCycle_Duration()==0. ) & ( plot_irrig_demand <= total_AvailableIrrigationWater ) ) {
                       double pond_flow = std::min(pond_AvailableIrrigationWater, plot_irrig_demand);
                       double pump_flow = plot_irrig_demand - pond_flow;
                       
                       pond_AvailableIrrigationWater -= pond_flow;
                       pump_AvailableIrrigationWater -= pump_flow;
                       
                       pond_extractions[indexplots[i]] += pond_flow;
                       pump_extractions[indexplots[i]] += pump_flow;
                       
                       total_AvailableIrrigationWater = pond_AvailableIrrigationWater + pump_AvailableIrrigationWater;
                       cycle_max_amount = PumpMaxVol()-std::accumulate(pump_extractions.begin(), pump_extractions.end(), 0.0) + pond_AvailableIrrigationWater;
                       
                       irrigation_amounts[indexplots[i]] += plot_irrig_demand;
                       is_day_without_irrigation[indexplots[i]] = 0.;
                       
                       if (DbgLog > 0.) std::cout << "\t\t\tAll water demand provided in a single day: " << irrigation_amounts[indexplots[i]] << " (m3)" << std::endl;
                       if (DbgLog > 1.) std::cout << "\t\t\t\tRemaining available supply is: " << total_AvailableIrrigationWater << " (m3) =\tfrom pond: " << pond_AvailableIrrigationWater << " (m3) +\tfrom pump: " << pump_AvailableIrrigationWater << " (m3) \tmax available with cycle: " << cycle_max_amount << " (m3)" << std::endl;

                   } else if ( ( WaterCycle_Duration()==0. ) & (total_AvailableIrrigationWater > 0.) & ( plot_irrig_demand <= cycle_max_amount ) & ( plot_cycle_duration <= (*Max_Irrigation_Durations[indexplots[i]])() ) ) {
                       WaterCycle_CurrentPlot = indexplots[i];
                       WaterCycle_Duration = plot_cycle_duration;
                       WaterCycle_RemainingAmount = plot_irrig_demand - total_AvailableIrrigationWater;
                       WaterCycle_CurrentDose = AvailableWater();
                       
                       pond_extractions[indexplots[i]] += pond_AvailableIrrigationWater;
                       pump_extractions[indexplots[i]] += pump_AvailableIrrigationWater;
                       
                       pond_AvailableIrrigationWater = 0.;
                       pump_AvailableIrrigationWater = 0.;
                       
                       total_AvailableIrrigationWater = pond_AvailableIrrigationWater + pump_AvailableIrrigationWater;
                       cycle_max_amount = PumpMaxVol() - std::accumulate(pump_extractions.begin(), pump_extractions.end(), 0.0) - WaterCycle_RemainingAmount() + pond_AvailableIrrigationWater;
                       
                       irrigation_amounts[indexplots[i]] += plot_irrig_demand;
                       is_day_without_irrigation[indexplots[i]] = 0.;
                       
                       if (DbgLog > 0.) std::cout << "\t\t\tStarting new water cycle for " << WaterCycle_Duration() << " days ( < " << (*Max_Irrigation_Durations[indexplots[i]])() << ")\tand dose " << WaterCycle_CurrentDose() << " (m3)\tRemainder: " << WaterCycle_RemainingAmount() << " (m3)" << std::endl;
                       
                   } else {
                        if (DbgLog > 0.) std::cout << "\t\t\tNo water provided" << std::endl;
                        if (DbgLog > 1.) {
                            if (!( total_AvailableIrrigationWater > 0. )) {
                                std::cout << "\t\t\t\tNo more water available for today..." << std::endl;
                            }
                            if ( plot_irrig_demand > cycle_max_amount ) {
                                std::cout << "\t\t\t\tMax pump volume is too low: "<< cycle_max_amount << " (m3) < " << plot_irrig_demand << " (m3)" << std::endl;
                            }
                            if (  plot_cycle_duration > (*Max_Irrigation_Durations[indexplots[i]])() ) {
                                std::cout << "\t\t\t\tWater cycle duration is too long: "<< plot_cycle_duration << " > " << (*Max_Irrigation_Durations[indexplots[i]])() << " (m3)" << std::endl;
                            }
                            if ( WaterCycle_Duration()!=0. ) {
                                std::cout << "\t\t\t\tWater cycle already running: "<< WaterCycle_Duration() << std::endl;
                            }
                       }
                   }
               } else {
                   is_day_without_irrigation[i] = 0.;
               }
           }

        } else {
            if (DbgLog > 0.) {
                std::cout << "\tNo water demand." <<std::endl;
            }

            WaterCycle_Duration = 0.;
            WaterCycle_RemainingAmount = 0.;
            WaterCycle_CurrentPlot = -1.;
            WaterCycle_CurrentDose = 0.;
            
            for (int i=0; i < nbBele; i++) {
                is_day_without_irrigation[i] = 0.;
            }
            
        }
    }

    for (int i=0; i < nbBele; i++) {
        *IrrigationAmounts[i] = irrigation_amounts[i];
        *Is_Day_Without_Irrigation[i] = ( (*Irrigation_Demands[i])() > 0. ) ? is_day_without_irrigation[i] : 0.;
        *PondExtractions[i] = pond_extractions[i];
        *PumpExtractions[i] = pump_extractions[i];
    }

    if (isRefill & (pump_AvailableIrrigationWater > 0.)) {
        PondRefill = std::min(pump_AvailableIrrigationWater, V0 - V(-1));
        
        if (DbgLog > 0.) {
            std::cout << "\tRefilling pond with remaining pump water: " << PondRefill() << " (m3)" <<std::endl;
        }
        if (DbgLog > 1.) {
            std::cout << "\t\tRemaining pump available: " << pump_AvailableIrrigationWater << " (m3)" <<std::endl;
            std::cout << "\t\tPond Vol to Max: " << V0 - V(-1) << " (m3)"  << "\tV0:" <<V0 << " (m3)\tV(t-1):" << V(-1) << " (m3)"<<std::endl;
        }

    } else {
        PondRefill = 0.;
    }

    PondExtraction = std::accumulate(pond_extractions.begin(), pond_extractions.end(), 0.0);
    PumpExtraction = std::accumulate(pump_extractions.begin(), pump_extractions.end(), 0.0) + PondRefill();

    for (int i=0; i < nbBele; i++) {
        if ((*IrrigationAmounts[i])() > 0.) {
            if (DbgLog > 1.) {
                std::cout << "\tUpdating Irrigation dose of plot "<< i+1<<"." <<std::endl;
            }
            *Irrigations[i] = convert_m3_to_mm((*IrrigationAmounts[i])(), PlotAreas[i]);
            if (DbgLog > 1.) {
                std::cout << "\t\t[IrrigationAmount: "<< (*IrrigationAmounts[i])() << " (m3)\tPlot_Area: " << PlotAreas[i] <<" (m2)]\t-> Irrigation: " << (*Irrigations[i])() <<" (mm)"<<std::endl;
            }
        } else {
            *Irrigations[i] = 0.;
        }
    }
    
    for (int i=0; i < nbBele; i++) {
        if ( (*Is_Day_Without_Irrigation[i])() > 0. ) {
            if (DbgLog > 1.) {
                std::cout << "\tIncrementing Days_Without_Irrigation of plot "<< i+1<<"." <<std::endl;
            }
            *Days_Without_Irrigation[i] = (*Days_Without_Irrigation[i])(-1) + 1.;
        } else {
            *Days_Without_Irrigation[i] = 0.;
        }
    }

    // print all daily state variables outputs
    if (DbgLog > 1.) {
        std::cout << "\tDaily state variables :" <<std::endl;
        print_all_outputs();
    }

}
    // inputs
    Var V;
    Var AvailableWater;
    Var PumpMaxVol;
    std::vector<Var*> Irrigation_Demands;
    std::vector<Var*> Max_Irrigation_Durations;
    std::vector<Var*> IrrigationPriorities;

    // state variables
    Var PondExtraction;
    Var PumpExtraction;
    Var PondRefill;
    Var WaterCycle_Duration;
    Var WaterCycle_RemainingAmount;
    Var WaterCycle_CurrentPlot;
    Var WaterCycle_CurrentDose;
    std::vector<Var*> IrrigationAmounts;
    std::vector<Var*> Irrigations;
    std::vector<Var*> Is_Day_Without_Irrigation;
    std::vector<Var*> Days_Without_Irrigation;
    std::vector<Var*> PondExtractions;
    std::vector<Var*> PumpExtractions;

    // parameters
    double DbgLog;
    int nbBele;
    bool isPondForIrrigation;
    bool isOptimize;
    bool isRefill;
    double V0;
    std::vector<double> PlotAreas;

private:

    Var* generate_Var(const vd::InitEventList& evts, std::string varname_prefix, int i, bool isSync, double dbglog) {
        std::string varname = varname_prefix+"_"+std::to_string(i+1);
        Var* r = new Var();
        r->init(this, varname, evts);
        if (isSync) getOptions().syncs.insert(std::make_pair(varname, 1)); // force sync for in input
        if (dbglog > 1.) std::cout << "\t\tAdding variable:\t" << varname << "\t(sync:" << (isSync?"true":"false") << ")" << std::endl;
        return(r);
    }

    // extract daily values from std::vector<Var*>
    std::vector<double> get_VarVector(std::vector<Var*> VarVector) {
        std::vector<double> doublevector;
        for(unsigned int i=0;i<VarVector.size();i++) {
            doublevector.push_back((*VarVector[i])());
        }
        return(doublevector);
    }

    double get_total_Irrigation_Demand(std::vector<double> IrrigationDemands, std::vector<double> PlotAreas) {
        double sum_of_elems = 0.;
        for(unsigned int i=0;i<IrrigationDemands.size();i++) {
            sum_of_elems += convert_mm_to_m3(IrrigationDemands[i], PlotAreas[i]);
        }
        return(sum_of_elems);
    }

    // convert water amounts integrated over area (m3) into water doses per unit area (mm)
    double convert_m3_to_mm(double m3Amount, double area) {
        return( (area!=0.) ? m3Amount * 1000. / area : 0.);
    }

    // convert water doses per unit area (mm) into water amounts integrated over area (m3)
    double convert_mm_to_m3(double mmDose, double area) {
        return(mmDose * area / 1000.);
    }

    void print_all_parameters() {
        std::cout << "\t\tDbgLog:\t" << DbgLog << std::endl;
        std::cout << "\t\tnbBele:\t" << nbBele << std::endl;
        std::cout << "\t\tisPondForIrrigation:\t" << (isPondForIrrigation?"true":"false") << std::endl;
        std::cout << "\t\tisOptimize:\t" << (isOptimize?"true":"false") << std::endl;
        std::cout << "\t\tisRefill:\t" << (isRefill?"true":"false") << std::endl;
        std::cout << "\t\tV0:\t" << V0 << std::endl;
        for (int i=0; i < nbBele; i++) {
            std::string plotarea_paramname = "PlotArea_"+std::to_string(i+1);
            std::cout << "\t\t"<< plotarea_paramname << ":\t" << PlotAreas[i] << "\t(m2)" << std::endl;
        }
    } 

    void print_all_inputs() {
        std::cout << "\t\tV(t-1):\t" << V(-1) << "\t(m3)" << std::endl;
        std::cout << "\t\tAvailableWater(t):\t" << AvailableWater() << "\t(m3)" << std::endl;
        std::cout << "\t\tPumpMaxVol(t):\t" << PumpMaxVol() << "\t(m3)" << std::endl;
        for (int i=0; i < nbBele; i++) {
            std::cout << "\t\tIrrigation_Demand_"<< i+1 << "(t):\t" << (*Irrigation_Demands[i])() << "\t(mm)" << std::endl;
            std::cout << "\t\tMax_Irrigation_Duration_"<< i+1 << "(t):\t" << (*Max_Irrigation_Durations[i])()  << "\t(day)"<< std::endl;
            std::cout << "\t\tIrrigationPriority_"<< i+1 << "(t):\t" << (*IrrigationPriorities[i])()  << "\t(-)"<< std::endl;
        }
    }

    void print_all_outputs() {
        std::cout << "\t\tPondExtraction(t):\t" << PondExtraction()  << "\t(m3)"<< std::endl;
        std::cout << "\t\tPumpExtraction(t):\t" << PumpExtraction()  << "\t(m3)"<< std::endl;
        std::cout << "\t\tPondRefill(t):\t" << PondRefill()  << "\t(m3)"<< std::endl;
        std::cout << "\t\tWaterCycle_Duration(t):\t" << WaterCycle_Duration()  << "\t(day)"<< std::endl;
        std::cout << "\t\tWaterCycle_RemainingAmount(t):\t" << WaterCycle_RemainingAmount()  << "\t(m3)"<< std::endl;
        std::cout << "\t\tWaterCycle_CurrentPlot(t):\t" << WaterCycle_CurrentPlot()  << "\t(-)"<< std::endl;
        std::cout << "\t\tWaterCycle_CurrentDose(t):\t" << WaterCycle_CurrentDose()  << "\t(m3)"<< std::endl;
        for (int i=0; i < nbBele; i++) {
            std::cout << "\t\tIrrigationAmount_"<< i+1 << "(t):\t" << (*IrrigationAmounts[i])()  << "\t(m3)" << std::endl;
            std::cout << "\t\tIrrigation_"<< i+1 << "(t):\t" << (*Irrigations[i])() << "\t(mm)" << std::endl;
            std::cout << "\t\tIs_Day_Without_Irrigation_"<< i+1 << "(t):\t" << (*Is_Day_Without_Irrigation[i])()  << "\t(bool)"<< std::endl;
            std::cout << "\t\tDays_Without_Irrigation_"<< i+1 << "(t):\t" << (*Days_Without_Irrigation[i])() << "\t(day)" << std::endl;
            std::cout << "\t\tPondExtraction_"<< i+1 << "(t):\t" << (*PondExtractions[i])()  << "\t(m3)"<< std::endl;
            std::cout << "\t\tPumpExtraction_"<< i+1 << "(t):\t" << (*PumpExtractions[i])()  << "\t(m3)"<< std::endl;
        }
    }

    void check_priorities_unicity(std::vector<double> irrigationpriorities, double dbglog) { // from https://stackoverflow.com/questions/46477764/check-stdvector-has-duplicates/46477901
        std::sort(irrigationpriorities.begin(), irrigationpriorities.end());
        auto it = std::unique( irrigationpriorities.begin(), irrigationpriorities.end() );
        bool wasUnique = (it == irrigationpriorities.end() );
        if (wasUnique) {
            if (dbglog > 1.) std::cout << "\t\tAll daily IrrigationPriorities are unique" <<std::endl;
        } else {
            if (dbglog > 1.) std::cout << "\t\tSome daily IrrigationPriorities are not unique" <<std::endl;
            throw vu::ModellingError(vu::format("[%s] Some daily IrrigationPriorities are not unique \n",
                    getModel().getCompleteName().c_str()));
        }
    }
    
    std::vector<int> get_ranking_index(const std::vector<double>& v_temp) { // from https://stackoverflow.com/questions/17554242/how-to-obtain-the-index-permutation-after-the-sorting
        std::vector<int> index(v_temp.size(), 0);
        for (unsigned int i = 0 ; i != index.size() ; i++) {
            index[i] = i;
        }
        std::sort(index.begin(), index.end(), [&](const int& a, const int& b) {return (v_temp[a] < v_temp[b]);});
        return index;
    }
};

} // namespace ASCROM

DECLARE_DYNAMICS(ASCROM::FarmIrrigationManager)

