###Options modifiables###

DEBUT_SIMULATION <- '1987-1-1';
DUREE_SIMULATION <- 20;

FICHIER_CLIMAT <- 'sampleInputs_Pond.txt';

X0 <- 10;
Y0 <- 10;
Z0 <- 5;
fx <- 1;
Init_Pond_Frac <- 0.;
Evap_coeff <- 0.5;
Percol_coeff <- 0.05;



########################


library("rvle");
library("chron");
library("readr")
options(chron.origin=c(month=11, day=24, year=-4713));
sessionInfo();

# lecture du vpz
mm <- new("Rvle", pkg="ASCROM", file="test_Pond.vpz");

# passage des vues en mode storage (pour recuperer les vues via la fonction results)
myOutputList <- rep("storage", length(getDefault(mm,"outputplugin")))
names(myOutputList) <- names(getDefault(mm,"outputplugin"))


  # lancement de la simulation avec forcage des paramètres
  mm <- setDefault(mm ,
            simulation_engine.begin_date=DEBUT_SIMULATION,
            simulation_engine.duration=DUREE_SIMULATION,
            cond_generic_with_header_complete.meteo_file=FICHIER_CLIMAT,
            condPond.X0=X0,
            condPond.Y0=Y0,
            condPond.Z0=Z0,
            condPond.fx=fx,
            condPond.Init_Pond_Frac=Init_Pond_Frac,
            condPond.Evap_coeff=Evap_coeff,
            condPond.Percol_coeff=Percol_coeff);
  
  saveVpz(mm, "tmp.vpz")
  
  mm <- run(mm, outputplugin=myOutputList)
  
  # valeurs des résultats (vue view)
  res <- results(mm)$view
  
  # Conversion of time into calendar date format
  res$dates <- dates(res$`top:FileReader.current_date_str`, format = c(dates = "y-m-d"));
  
  # exemple de graph sur les sorties
  for (VarName in colnames(res)) {
    if (!VarName%in%c("time", "dates", "top:FileReader.current_date_str")) {
      plot(res$dates, res[, VarName==colnames(res)], main=VarName, type="l")
    }
  }
  