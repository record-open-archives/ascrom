###Options modifiables###
DO_EXPORT <- TRUE

DEBUT_SIMULATION <- '1987-1-1';
DUREE_SIMULATION <- 200;

FICHIER_CLIMAT <- 'sampleInputs.txt';

Irrig_Rule_Strat <- "None" #{None, Rain_Threshold, SoilWaterContent_Threshold, CropWaterStress_Threshold}

fw <- 1.0;
fw_Rain_Threshold <- 5.0;
REW_AWC_ratio <- 2/3;

Failure_Option <- 0; #{0:none, 1:water, 2:stress, 3:water&stress, 4:water|stress}
Average_CropWaterStress_WindowSpan <- 5;

RunOff_CurveNumber <- 85.0;
RunOff_Ia_SoilType_param <- 0.2;

Plot_Area <- 10000; # surface de la plot en m²

#IrrigationPriority
a <- 0.4
b <- 0
c <- 0.3
d <- 0.3
RS_strategy <- 1
RC_strategy <- 1

#FarmIrrigationManager
isPondForIrrigation <- 1;
isOptimize <- 1;
isRefill <- 1;
V0 <- 500;


KC_DATADIR <- '../../ASCROM/data/crops/kc/';
KC_DATA <- data.frame(id=c("ex1", "ex2"), 
                      filename=c("kc_sample.txt", "kc_sample2.txt"),
                      stringsAsFactors=FALSE)
KC_T0 <- 0;
KC_SEQ <- data.frame(sequence=c("ex1", "ex2", "ex1"), 
                     padding=0,
                     startdate=c("2/3/1987", "3/4/1987", "10/6/1987"),
                     enddate=c("2/4/1987", "4/5/1987", "11/7/1987"),
                     stringsAsFactors=FALSE)

#liste des paramètres attendus dans les fichiers sol: TH1, ThetaFC1, ThetaWP1, BD1, TH2, ThetaFC2, ThetaWP2, BD2, REW1, REW2, Rain_Threshold
SOIL_DATADIR <- "../data/soils/";
SOIL_ID <- "sol_Berambadi"; #repose sur l'existance d'un fichier {SOIL_ID}.txt dans le dossier {SOIL_DATADIR}

CROP_DATAFILE <- '../../ASCROM/data/crops/crop_parameters.txt';

if (DO_EXPORT) save(list=ls(), file="ascrom_params.rdata")

########################


library("rvle");
library("chron");
library("readr")
options(chron.origin=c(month=11, day=24, year=-4713));
sessionInfo();

# lecture du vpz
mm <- new("Rvle", pkg="ASCROM", file="example_ASCROM.vpz");

# passage des vues en mode storage (pour recuperer les vues via la fonction results)
myOutputList <- rep("storage", length(getDefault(mm,"outputplugin")))
names(myOutputList) <- names(getDefault(mm,"outputplugin"))


{## modifications auto de kc_params, h_params à partir de la séquence utilisateur
  
  all_crop_params <- read_csv2(CROP_DATAFILE, col_types=cols(CropID = col_character(), h = col_double()));
  
  KC_LIST <- sapply(unique(KC_SEQ$sequence), 
                    function(kc_ids){read_csv2(paste0(KC_DATADIR, KC_DATA$filename[KC_DATA$id==kc_ids]), col_types=cols(date = col_integer(), kc = col_double()));},
                    simplify=FALSE, USE.NAMES=TRUE)
  
  getSamplePair <- function(RvleObj, condPortName, T0) {
    sample_pair <- getDefault(RvleObj, condPortName);
    sample_pair$t0 <- as.integer(T0)
    for (i in 2:length(sample_pair$pairs)) {
      sample_pair$pairs[[2]] <- NULL
    }
    return(sample_pair)
  }
  
  
  generateKcPairs <- function(RvleObj, condPortName, T0, crop_seq, kc_list, debut_sim) {
    error_kc <- FALSE
    kc_params <- getSamplePair(RvleObj, condPortName, T0)
    sample_kc_pair <- kc_params$pairs[[1]]

    #generation auto de la sequence complete de paires à partir de la sequence d'id
    kc_pair_index <- 1 # index intégré de la paire kc en cours
    padding <- 0 # decallage intégré (sequence + padding) pour la sequence en cours 
    for (idseq_index in 1:nrow(crop_seq)) {#idseq_index=1
      if (idseq_index==1) {
        crop_seq$padding[idseq_index] <- as.integer(chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y")) - chron(debut_sim, format=c(dates = "Y-m-d")))
      } else {
        crop_seq$padding[idseq_index] <- as.integer(chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y")) - chron(crop_seq$enddate[idseq_index-1], format=c(dates = "d/m/Y")))
      }
      {
        if (crop_seq$padding[idseq_index]<1) {
          if (idseq_index==1) {
            cat("Attention date de debut de la séquence avant le début de simulation\n")
            cat(as.character(chron(debut_sim, format=c(dates = "Y-m-d"), out.format=c(dates = "d/m/Y"))), " < ", as.character(chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y"))), "\n")
          } else {
            cat("Attention date de debut avant date de fin du précédent\n")
            cat(as.character(chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y"))), " < ", as.character(chron(crop_seq$enddate[idseq_index-1], format=c(dates = "d/m/Y"))), "\n",
                idseq_index, " vs ", idseq_index-1, " (element de la séquence)\n")
          }
          error_kc <- TRUE;
        }
        dureeDates <- as.integer(chron(crop_seq$enddate[idseq_index], format=c(dates = "d/m/Y")) - chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y")))
        dureePaires <- max(kc_list[[crop_seq$sequence[idseq_index]]]$date)
        if (dureeDates!=dureePaires) {
          cat("Attention durée différentes entre dates et fichiers de kc\n")
          cat(idseq_index, "eme element de la séquence, ID:", crop_seq$sequence[idseq_index], "\n")
          cat(dureeDates, "(dates séquence) != ", dureePaires, "(durée paires)\n")
          error_kc <- TRUE;
        }
      }
      padding <- padding+crop_seq$padding[idseq_index]
      for (idpair_index in 1:nrow(kc_list[[crop_seq$sequence[idseq_index]]])) {#idpair_index=1
        kc_params$pairs[[kc_pair_index]] <- sample_kc_pair
        kc_params$pairs[[kc_pair_index]]$kcb <- kc_list[[crop_seq$sequence[idseq_index]]][idpair_index,]$kc
        kc_params$pairs[[kc_pair_index]]$date <- as.integer(kc_list[[crop_seq$sequence[idseq_index]]][idpair_index,]$date + padding)
        kc_pair_index <- kc_pair_index+1
      }
      
      padding <- padding+max(kc_list[[crop_seq$sequence[idseq_index]]]$date)
    }
    
    return(list(KC_PARAMS=kc_params,
                KC_SEQ=crop_seq,
                ERROR_KC=error_kc));
    
  }

  generatePairParameters <- function(RvleObj, condPortName, mapKeyName, T0, crop_params_datas, crop_seq, kc_pairs) {#RvleObj=mm; condPortName="condClimaticWaterDemand.h_params"; mapKeyName="h"; T0=KC_T0; crop_params_datas=all_crop_params; crop_seq=KC_SEQ; kc_pairs=KC_LIST;
    
    XX_params <- getSamplePair(RvleObj, condPortName, T0);
    sample_XX_pair <- XX_params$pairs[[1]];

    padding <- 0; # decallage intégré (sequence + padding) pour la sequence en cours 
    
    for (idseq_index in 1:nrow(crop_seq)) {#idseq_index=1
      padding <- padding+crop_seq$padding[idseq_index];
      
      XX_params$pairs[[(idseq_index-1)*4+1]] <- sample_XX_pair;
      XX_params$pairs[[(idseq_index-1)*4+1]][[mapKeyName]] <- 0;
      XX_params$pairs[[(idseq_index-1)*4+1]]$date <- as.integer(kc_pairs[[crop_seq$sequence[idseq_index]]][1,]$date + padding);
      
      XX_params$pairs[[(idseq_index-1)*4+2]] <- sample_XX_pair;
      XX_params$pairs[[(idseq_index-1)*4+2]][[mapKeyName]] <- crop_params_datas[[mapKeyName]][crop_params_datas$CropID==crop_seq$sequence[idseq_index]];
      XX_params$pairs[[(idseq_index-1)*4+2]]$date <- as.integer(kc_pairs[[crop_seq$sequence[idseq_index]]][1,]$date + padding+1);
      
      XX_params$pairs[[(idseq_index-1)*4+3]] <- sample_XX_pair;
      XX_params$pairs[[(idseq_index-1)*4+3]][[mapKeyName]] <- crop_params_datas[[mapKeyName]][crop_params_datas$CropID==crop_seq$sequence[idseq_index]];
      XX_params$pairs[[(idseq_index-1)*4+3]]$date <- as.integer(kc_pairs[[crop_seq$sequence[idseq_index]]][nrow(kc_pairs[[crop_seq$sequence[idseq_index]]]),]$date + padding-1);

      XX_params$pairs[[(idseq_index-1)*4+4]] <- sample_XX_pair;
      XX_params$pairs[[(idseq_index-1)*4+4]][[mapKeyName]] <- 0;
      XX_params$pairs[[(idseq_index-1)*4+4]]$date <- as.integer(kc_pairs[[crop_seq$sequence[idseq_index]]][nrow(kc_pairs[[crop_seq$sequence[idseq_index]]]),]$date + padding);

      padding <- padding+max(kc_pairs[[crop_seq$sequence[idseq_index]]]$date);
    }

    XX_params$pairs[sapply(XX_params$pairs, is.null)] <- NULL

    return(XX_params);
  }
  
  KC_FUN <- generateKcPairs(mm, "condInterpolator.kcb_params", KC_T0, KC_SEQ, KC_LIST, DEBUT_SIMULATION); 
  KC_PARAMS <- KC_FUN$KC_PARAMS
  KC_SEQ <- KC_FUN$KC_SEQ
  ERROR_KC <- KC_FUN$ERROR_KC
  all_crop_params$Crop_Cycle_Duration <- sapply(KC_LIST[all_crop_params$CropID], function(x){as.numeric(x$date[6]-x$date[2])}) #todo: fix key order
  
  
  h_params <- generatePairParameters(mm, "condInterpolator.h_params", "h", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  MaxDepthRoots_params <- generatePairParameters(mm, "condInterpolator.MaxDepthRoots_params", "MaxDepthRoots", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  kcini_params <- generatePairParameters(mm, "condInterpolator.kcini_params", "kcini", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  kcmid_params <- generatePairParameters(mm, "condInterpolator.kcmid_params", "kcmid", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  
  IrrigationDose_params <- generatePairParameters(mm, "condInterpolator.IrrigationDose_params", "IrrigationDose", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  Max_Irrigation_Duration_params <- generatePairParameters(mm, "condInterpolator.Max_Irrigation_Duration_params", "Max_Irrigation_Duration", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  Rain_Threshold_params <- generatePairParameters(mm, "condInterpolator.Rain_Threshold_params", "Rain_Threshold", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  SoilWaterContent_Threshold_params <- generatePairParameters(mm, "condInterpolator.SoilWaterContent_Threshold_params", "SoilWaterContent_Threshold", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  CropWaterStress_Threshold_params <- generatePairParameters(mm, "condInterpolator.CropWaterStress_Threshold_params", "CropWaterStress_Threshold", KC_T0, all_crop_params, KC_SEQ, KC_LIST)

  YieldMax_params <- generatePairParameters(mm, "condInterpolator.YieldMax_params", "YieldMax", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  Rain_Threshold_To_Reset_Days_Without_Water_params <- generatePairParameters(mm, "condInterpolator.Rain_Threshold_To_Reset_Days_Without_Water_params", "Rain_Threshold_To_Reset_Days_Without_Water", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  Lethal_Threshold_Days_Without_Water_params <- generatePairParameters(mm, "condInterpolator.Lethal_Threshold_Days_Without_Water_params", "Lethal_Threshold_Days_Without_Water", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  Lethal_Threshold_CropWaterStress_params <- generatePairParameters(mm, "condInterpolator.Lethal_Threshold_CropWaterStress_params", "Lethal_Threshold_CropWaterStress", KC_T0, all_crop_params, KC_SEQ, KC_LIST)

  CropPriority_params <- generatePairParameters(mm, "condInterpolator.CropPriority_params", "CropPriority", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  Irrigation_Technic_Factor_params <- generatePairParameters(mm, "condInterpolator.Irrigation_Technic_Factor_params", "Irrigation_Technic_Factor", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
  Crop_Cycle_Duration_params <- generatePairParameters(mm, "condInterpolator.Crop_Cycle_Duration_params", "Crop_Cycle_Duration", KC_T0, all_crop_params, KC_SEQ, KC_LIST)
}

{##recupération des paramètres sol à partir de l'identifiant
  
  soil_list <- read_csv2(paste0(SOIL_DATADIR, SOIL_ID, ".txt"), col_types=cols(ParamName = col_character(), ParamValue = col_double()));
  
  sapply(soil_list$ParamName, function(x){assign(x, soil_list$ParamValue[soil_list$ParamName==x], envir = .GlobalEnv)})
}

{
  AWC1 <- (ThetaFC1-ThetaWP1)*BD1*TH1;
  REW1 <- REW_AWC_ratio * AWC1;
  AWC2 <- (ThetaFC2-ThetaWP2)*BD2*TH2;
  REW2 <- REW_AWC_ratio * AWC2;
}


if (!ERROR_KC) {
  
  # lancement de la simulation avec forcage des paramètres
  mm <- setDefault(mm ,
            simulation_engine.begin_date=DEBUT_SIMULATION,
            simulation_engine.duration=DUREE_SIMULATION,
            cond_generic_with_header_complete.meteo_file=FICHIER_CLIMAT,
            condInterpolator.kcb_params.as_single=KC_PARAMS,
            condInterpolator.h_params.as_single=h_params,
            condInterpolator.MaxDepthRoots_params.as_single=MaxDepthRoots_params, 
            condInterpolator.kcini_params.as_single=kcini_params, 
            condInterpolator.kcmid_params.as_single=kcmid_params, 
            condClimaticWaterDemand.Failure_Option=as.integer(Failure_Option),
            condInterpolator.Rain_Threshold_To_Reset_Days_Without_Water_params.as_single=Rain_Threshold_To_Reset_Days_Without_Water_params,
            condInterpolator.Lethal_Threshold_Days_Without_Water_params.as_single=Lethal_Threshold_Days_Without_Water_params,
            condInterpolator.Lethal_Threshold_CropWaterStress_params.as_single=Lethal_Threshold_CropWaterStress_params,
            condWaterFlows.CurveNumber=RunOff_CurveNumber, 
            condWaterFlows.Ia_SoilType_param=RunOff_Ia_SoilType_param, 
            condActualIrrigation.Irrig_Efficiency=fw, 
            condClimaticWaterDemand.fw=fw, 
            condClimaticWaterDemand.fw_Rain_Threshold=fw_Rain_Threshold, 
            condClimaticWaterDemand.SoilDepth=TH1+TH2, 
            condClimaticWaterDemand.REW1=REW1, 
            condClimaticWaterDemand.REW2=REW2, 
            condWaterFlows.TH1=TH1,
            condWaterFlows.ThetaFC1=ThetaFC1,
            condWaterFlows.ThetaWP1=ThetaWP1,
            condWaterFlows.BD1=BD1,
            condWaterFlows.TH2=TH2,
            condWaterFlows.ThetaFC2=ThetaFC2,
            condWaterFlows.ThetaWP2=ThetaWP2,
            condWaterFlows.BD2=BD2,
            condIrrigationDemand.Rule_Strat=Irrig_Rule_Strat,
            condInterpolator.IrrigationDose_params.as_single=IrrigationDose_params,
            condInterpolator.Max_Irrigation_Duration_params.as_single=Max_Irrigation_Duration_params,
            condInterpolator.Rain_Threshold_params.as_single=Rain_Threshold_params,
            condInterpolator.SoilWaterContent_Threshold_params.as_single=SoilWaterContent_Threshold_params,
            condInterpolator.CropWaterStress_Threshold_params.as_single=CropWaterStress_Threshold_params,
            condMovingAverage.n=as.integer(Average_CropWaterStress_WindowSpan),
            condInterpolator.YieldMax_params.as_single=YieldMax_params,
            condInterpolator.Irrigation_Technic_Factor_params.as_single=Irrigation_Technic_Factor_params,
            condInterpolator.Crop_Cycle_Duration_params.as_single=Crop_Cycle_Duration_params,
            condInterpolator.CropPriority_params.as_single=CropPriority_params,
            condIrrigationPriority.a=a,
            condIrrigationPriority.b=b,
            condIrrigationPriority.c=c,
            condIrrigationPriority.d=d,
            condIrrigationPriority.RS_strategy=as.integer(RS_strategy),
            condIrrigationPriority.RC_strategy=as.integer(RC_strategy),
            condFarmIrrigationManager.isPondForIrrigation=as.logical(isPondForIrrigation),
            condFarmIrrigationManager.isOptimize=as.logical(isOptimize),
            condFarmIrrigationManager.isRefill=as.logical(isRefill),
            condFarmIrrigationManager.V0=V0,
            condFarmIrrigationManager.PlotArea_1=Plot_Area);
  
  saveVpz(mm, "tmp.vpz")
  
  mm <- run(mm, outputplugin=myOutputList)
  
  # valeurs des résultats (vue view)
  res <- results(mm)$view
  
  # Conversion of time into calendar date format
  res$dates <- dates(res$`top:FileReader.current_date_str`, format = c(dates = "y-m-d"));
  
  # exemple de graph sur les sorties
  filter_regex <- "*"
  for (VarName in colnames(res)[grep(filter_regex, colnames(res))]) {
    if (!VarName%in%c("time", "dates", "top:FileReader.current_date_str")) {
      plot(res$dates, res[, VarName==colnames(res)], main=VarName, ylab=tail(strsplit(x=VarName, split=".", fixed=TRUE)[[1]], n=1), type="l")
    }
  }
  
  
  if(FALSE){
    
    print_pair_params <- function(XX_params) {#XX_params=YieldMax_params
      cat("t0=", XX_params$t0, "\n")
      cat("Number of pair values: ", length(XX_params$pairs), "\n")
      for (i in 1:length(XX_params$pairs)) {
        cat(i, " -\t");
        for (j in names(XX_params$pairs[[i]])) {
          cat(j, ": ", XX_params$pairs[[i]][[j]], "\t")
        }
        cat("\n")
      }
    }
    print_pair_params(KC_PARAMS)
    print_pair_params(h_params)
    print_pair_params(YieldMax_params)
    print_pair_params(CropPriority_params)
    print_pair_params(Crop_Cycle_Duration_params)
    print_pair_params(Irrigation_Technic_Factor_params)
    
  
  library("dygraphs"); #dygraph(...) : interactive plot of time series
  library("xts"); #xts(...) : time series class for dygraph
  
  # Conversion of time into calendar date format
  res$time <- dates(res$`top:FileReader.current_date_str`, format = c(dates = "y-m-d"));
  
  xts_time <- as.POSIXct(as.character(res$time), format="%m/%d/%y")
  mycol <- "darkblue";
  
  
  p <- list()
  for (VarName in colnames(res)) {
    if (!VarName%in%c("time", "dates", "top:FileReader.current_date_str")) {
      Var <- res[, VarName==colnames(res)]
      p[[VarName]] <- dygraph(xts(Var, xts_time), ylab=VarName, main=VarName)%>%
        dyOptions(fillGraph = TRUE, fillAlpha = 0.4) %>%
        dySeries("V1", label=VarName, drawPoints = TRUE, color = mycol);
      
    }
  }
  p

  }
  
}
############################
#resultats dans fichier
############################
#sink("/home/record/Documents/R/Result/nom_fichier.csv")
#print(resultat_voulu)

############################
#resultats sur ecran
############################
#sink()
#print(resultat_voulu)
