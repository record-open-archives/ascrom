



X0 <- 16;
Y0 <- 16;
Z0 <- 3;
fx <- 2;

Init_Pond_Frac <- seq(from=0., to=1., by=0.01);



fy = Y0 * fx / X0;
S0 = X0 * Y0;
X1 = X0 - fx * Z0;
Y1 = Y0 - fy * Z0;
Z1 = X0 / fx;
S1 = X1 * Y1;
V0 = Z0 * (S0 + S1 + sqrt(S0 * S1)) / 3.;
V1 = (Z1 - Z0)^3 * fx * fy / 3.;



get_Z <- function(V) {
  return( Z0 - Z1 + (3. * (V + V1) / (fx * fy))^(1/3));
}


V <- Init_Pond_Frac*V0;
Z <- get_Z(Init_Pond_Frac*V0);

plot(V, Z, type="l", xlab="Volume (m^3)", ylab="Z (m)")
plot(Z, V, type="l", ylab="Volume (m^3)", xlab="Z (m)")




X <- (Z + Z1 - Z0) * fx;
Y <- (Z + Z1 - Z0) * fy;
S <- X * Y;

plot(V, X, type="l", xlab="Volume (m^3)", ylab="X (m)")
plot(V, Y, type="l", xlab="Volume (m^3)", ylab="Y (m)")
plot(V, S, type="l", xlab="Volume (m^3)", ylab="S (m^2)")


