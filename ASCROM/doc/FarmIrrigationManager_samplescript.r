###Options modifiables###

DEBUT_SIMULATION <- '1987-1-1';
DUREE_SIMULATION <- 4;

FICHIER_CLIMAT <- 'sampleInputs_FarmIrrigationManager.txt';


isPondForIrrigation <- 1;
isOptimize <- 1;
isRefill <- 1;

V0 <- 500;

PlotArea_1 <- 2500;
PlotArea_2 <- 2500;
PlotArea_3 <- 2500;
PlotArea_4 <- 2500;



########################


library("rvle");
library("chron");
library("readr")
options(chron.origin=c(month=11, day=24, year=-4713));
sessionInfo();

# lecture du vpz
mm <- new("Rvle", pkg="ASCROM", file="test_FarmIrrigationManager.vpz");

# passage des vues en mode storage (pour recuperer les vues via la fonction results)
myOutputList <- rep("storage", length(getDefault(mm,"outputplugin")))
names(myOutputList) <- names(getDefault(mm,"outputplugin"))


  # lancement de la simulation avec forcage des paramètres
  mm <- setDefault(mm ,
            simulation_engine.begin_date=DEBUT_SIMULATION,
            simulation_engine.duration=DUREE_SIMULATION,
            cond_generic_with_header_complete.meteo_file=FICHIER_CLIMAT,
            condFarmIrrigationManager.isPondForIrrigation=as.logical(isPondForIrrigation),
            condFarmIrrigationManager.isOptimize=as.logical(isOptimize),
            condFarmIrrigationManager.isRefill=as.logical(isRefill),
            condFarmIrrigationManager.V0=V0,
            condFarmIrrigationManager.PlotArea_1=PlotArea_1,
            condFarmIrrigationManager.PlotArea_2=PlotArea_2,
            condFarmIrrigationManager.PlotArea_3=PlotArea_3,
            condFarmIrrigationManager.PlotArea_4=PlotArea_4);
  
  saveVpz(mm, "tmp.vpz")
  
  mm <- run(mm, outputplugin=myOutputList)
  
  # valeurs des résultats (vue view)
  res <- results(mm)$view
  
  # Conversion of time into calendar date format
  res$dates <- dates(res$`top:FileReader.current_date_str`, format = c(dates = "y-m-d"));
  
  # exemple de graph sur les sorties
  filter_regex <- "FarmIrrigationManager"
  for (VarName in colnames(res)[grep(filter_regex, colnames(res))]) {
    if (!VarName%in%c("time", "dates", "top:FileReader.current_date_str")) {
      plot(res$dates, res[, VarName==colnames(res)], main=VarName, ylab=tail(strsplit(x=VarName, split=".", fixed=TRUE)[[1]], n=1), type="l")
    }
  }
  
