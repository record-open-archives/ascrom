---
title: "Pond model proposal for the ASCROM model"
---

<!-- Equation (using $$..$$) automatic numbering, use the tag \nonumber to deactivate for an individual equation -->
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { 
      equationNumbers: { 
            autoNumber: "all",
            formatNumber: function (n) {return 'Eq.'+n}
      } 
  }
});
</script>

# Pond model proposal

The shape of the pond is a reversed truncated pyramid with a rectangular shaped base :

![pond_figure ](figures/pond.png)


--- 

#### User Parameters settings 

| Parameter name | Type | Is mandatory? | Description&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; |
| -- | :--: | :--: | -- |
| **X0** | double | [x] | Pond surface Length (m) |
| **Y0** | double | [x] | Pond surface Width (m) |
| **Z0** | double | [x] | Pond depth (m) |
| **fx** | double | [x] | Shape coefficient for X dimension (must be positive) (m/m) |
| ~~**fy**~~ | ~~double~~ | ~~[x]~~ | ~~Shape coefficient for Y dimension (must be positive) (m/m)~~ |
| **Evap_coeff** | double | [x] | Pond evaporation reduction coefficient (??) |
| **Percol_coeff** | double | [x] | Pond percolation coefficient (??) |
| **Init_Pond_Frac** | double | [x] | Pond initial volume as a fraction of V0 (-) |

> ***Rmq : il y a trop de paramètres, en effet on a aussi par construction (venant du fait qu'on veux une forme de pyramide) une relation entre X0, Y0, fx et fy :  
$\frac{X0}{fx} = \frac{Y0}{fy} = Z1$  
On peut donc deduire la valeur de fy à partir des valeurs de X0, Y0 et fx par :  
$fy = \frac{Y0* fx}{X0}$***

#### Local parameters

The following are computed once from user parameters and then keep a fixed value during the simulation duration  

| Parameter name | Type | Description&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; |
| -- | :--: | -- |
| **fy** | double | Shape coefficient for Y dimension (m/m) |
| **S0** | double | Pond surface area (m<sup>2</sup>) |
| **X1** | double | Pond bottom Length (m) |
| **Y1** | double | Pond bottom Width (m) |
| **Z1** | double | Pyramid Apex depth (m) |
| **S1** | double | Pond bottom area (m<sup>2</sup>) |
| **V0** | double | Pond total volume (m<sup>3</sup>) |
| **V1** | double | Trunk volume of the truncated pyramid (m<sup>3</sup>) |

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#LocalParam_panel'>Show/Hide Local parameters equations</button>
<div id="LocalParam_panel" class="collapse">

> $$fy = \frac{Y0 * fx}{X0}
\label{eq:fy}$$

> $$S0 = X0 * Y0
\label{eq:S0}$$

> $$X1 = X0 - fx * Z0
\label{eq:X1}$$

> $$Y1 = Y0 - fy * Z0
\label{eq:Y1}$$

> $$Z1 = \frac{X0}{fx} = \frac{Y0}{fy}
\label{eq:Z1}$$

> $$S1 = X1 * Y1
\label{eq:S1}$$

> $$V0 = \frac{1}{3} * Z0 * (S0 + S1 + \sqrt{S0 * S1})
\label{eq:V0}$$

> $$V1 = \frac{1}{3} * (Z1 - Z0) * (S0 + S1 + \sqrt{S0 * S1}) = \frac{1}{3} * (Z1 - Z0)^3 * fx * fy
\label{eq:V1}$$

</div>


#### Input settings 

* **ET0 :** Reference crop evapotranspiration (We recomend to use values calculated by the Penman Monteith equation as it is done in FAO approach) (mm.d<sup>-1</sup>) (double) (sync)  
* **Rain :** daily rain (mm) (double) (sync)  
* **RunOff :** daily incoming surface runoff volume (m<sup>3</sup>) (double) (nosync)  
* **Pond_Irrigation :** daily actual irrigation using pond water (m<sup>3</sup>) (double) (sync)  
* **AvailableWater :** daily available water for refilling the pond (m<sup>3</sup>) (double) (sync)  

> ***Rmq : les volumes (unités : m<sup>3</sup>), a intégrer sur les surfaces des parcelles de l'exploitation en amont lors du couplage***


#### Output settings 

* **Evaporation :** Pond daily surface Evaporation (m<sup>3</sup>) (double)
* **Percolation :** Pond daily bottom Percolation (m<sup>3</sup>) (double)
* **Pump_water_refill :** Pond daily Refill volume (m<sup>3</sup>) (double)
* **Pond_Overflow : ** Pond water overflow loss (m<sup>3</sup>) (double)
* **V :** Pond water volume (m<sup>3</sup>) (double)
* **X :** Pond water Length (m) (double)
* **Y :** Pond water Width (m) (double)
* **S :** Pond water area (m<sup>2</sup>) (double)
* **Z :** Pond water height (m) (double)
  

### Details 

#### State variables equations 

> $$Evaporation(t) = ET0(t) * Evap\_coeff * S(t-1)
\label{eq:Evaporation}$$

> $$Percolation(t) = Z(t-1) * S1 * Percol\_coeff
\label{eq:Percolation}$$

> $$Pump\_water\_refill(t)=\min\left(
  \begin{array}{@{}ll@{}}
    V0  - \min\left(
  \begin{array}{@{}ll@{}}
    V0 \\
    (V(t-1) + Rain(t) * S0 + RunOff(t-1) - Pond\_Irrigation(t) - Evaporation(t) - Percolation(t))
  \end{array}\right) \\
     AvailableWater(t)
  \end{array}\right)
\label{eq:Pump_water_refill}$$


> $$Pond\_Overflow(t)=\max\left(
  \begin{array}{@{}ll@{}}
    0 \\
    (V(t-1) + Rain(t) * S0 + RunOff(t-1) - Pond\_Irrigation(t) - Evaporation(t) - Percolation(t)) - V0
  \end{array}\right)
\label{eq:Pump_Overflow}$$

> $$V(t) = V(t-1) + Rain(t) * S0 + Runoff(t-1) + Pump\_water\_refill(t) - Pond\_Overflow(t) - Pond\_Irrigation(t) - Evaporation(t) - Percolation(t) 
\label{eq:V}$$  
and initialized with :
$V(0) = Init\_Pond\_Frac * V0$

> $$Z(t) = Z0 - Z1 + \sqrt[3]{\frac{3 * (V(t) + V1)}{fx * fy}}
\label{eq:Z}$$

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Z_panel'>Show/Hide Z(t) equation details</button>
<div id="Z_panel" class="collapse">

> $X(t) = (Z(t) + Z1 - Z0) * fx$  
$Y(t) = (Z(t) + Z1 - Z0) * fy$  
$S(t) = X(t) * Y(t) = (Z(t) + Z1 - Z0)^2 * fx * fy$  
$V(t) = \frac{1}{3} * S(t) * (Z(t) + Z1 - Z0) - V1 = \frac{1}{3} * (Z(t) + Z1 - Z0)^3 * fx * fy - V1$  
$(Z(t) + Z1 - Z0)^3 =\frac{3 * (V(t) + V1)}{fx * fy}$  

</div>

> $$X(t) = (Z(t) + Z1 - Z0) * fx
\label{eq:X}$$

> $$Y(t) = (Z(t) + Z1 - Z0) * fy
\label{eq:Y}$$

> $$S(t) = X(t) * Y(t) = (Z(t) + Z1 - Z0)^2 * fx * fy
\label{eq:S}$$


...



