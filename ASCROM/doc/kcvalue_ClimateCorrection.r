###Options modifiables###

DEBUT_SIMULATION <- "2005-1-1";
DUREE_SIMULATION <- 365*5;

FICHIER_CLIMAT <- "../data/data_maddur.txt"; #liste des donnée meteo attendues: ETP, Rain, AvailableWater
CROP_DATAFILE <- "../data/crops/crop_parameters_mariem.txt"; #liste des paramètres culturaux attendus dans le fichier: h 

KC_DATADIR <- "../data/crops/kc/"
KC_DATA <- data.frame(id=c("banana", "beetroot", "bengalgram", "cabbage", "cowpeas", "finger_millet", "groundnut", "Horsegram", "maize_k", "maize_r", "onion", "sorghum", "sugarcane", "sunflower"), 
                      filename=c("kc_banana.txt", "kc_beetroot.txt", "kc_bengalgram.txt", "kc_cabbage.txt", "kc_cowpeas.txt", "kc_finger_millet.txt", "kc_groundnut.txt", "kc_Horsegram.txt", "kc_maize_k.txt", "kc_maize_r.txt", "kc_onion.txt", "kc_sorghum.txt", "kc_sugarcane.txt", "kc_sunflower.txt"), 
                      stringsAsFactors=FALSE);
KC_T0 <- 0;
KC_SEQ <- data.frame(sequence=c("sunflower", "sorghum", "cabbage", "maize_k", "onion", "beetroot", "cabbage", "sugarcane", "beetroot", "maize_k", "beetroot", "sugarcane", "banana"), 
                     padding=0, 
                     startdate=c("01/05/2005", "01/06/2006", "01/01/2007", "01/05/2008", "01/04/2009", "01/09/2009", "01/01/2010", "01/09/2010", "01/09/2011", "01/05/2012", "01/09/2013", "01/09/2014", "01/09/2015"), 
                     enddate=c("09/09/2005", "10/10/2006", "16/06/2007", "04/09/2008", "06/07/2009", "01/12/2009", "16/06/2010", "19/07/2011", "01/12/2011", "04/09/2012", "01/12/2013", "19/07/2015", "26/09/2016"),
                     stringsAsFactors=FALSE);

NEWFILES_PREFIX <- "CC_";

########################

library("readr");
library("rvle");
library("chron");
options(chron.origin=c(month=11, day=24, year=-4713));
sessionInfo();


weatherdata <- read_delim(FICHIER_CLIMAT, delim=" ", col_types=cols_only(julien= col_integer(), annee= col_integer(), mois= col_integer(), jour= col_integer(), RHmin= col_double(), u2= col_double()));

all_crop_params <- read_csv2(CROP_DATAFILE, col_types=cols(CropID = col_character(), h = col_double()));

date_debut <- chron(DEBUT_SIMULATION, format=c(dates = "y-m-d"));

weatherdates <- chron(paste(weatherdata$annee, weatherdata$mois, weatherdata$jour, sep="-"),format=c(dates = "y-m-d"));

new_KC_DATA <- KC_DATA;
new_KC_SEQ <- KC_SEQ;

CLIM_PREFIX <- paste0(tools::file_path_sans_ext(basename(FICHIER_CLIMAT)), "_");

error_kc <- FALSE;
kc_list <- list();
kcdates_list <- list();
RHmin_mean <- list();
u2_mean <- list();
h <- list();
#lecture des fichiers de paires requis
for(kc_ids in unique(KC_SEQ$sequence)) { #kc_ids=unique(KC_SEQ$sequence)[1]
  kc_list[[kc_ids]] <- read_csv2(file.path(KC_DATADIR, KC_DATA$filename[KC_DATA$id==kc_ids]), col_types=cols(date = col_integer(), kc = col_double()));
}
new_kc_list <- kc_list

kc_pair_index <- 1 # index intégré de la paire kc en cours
padding <- 0 # decallage intégré (sequence + padding) pour la sequence en cours 
for (idseq_index in 1:nrow(KC_SEQ)) {#idseq_index=1
  id_crop <- KC_SEQ$sequence[idseq_index];
  DATE_PREFIX <- paste0("from_", chron(KC_SEQ$startdate[idseq_index], format=c(dates = "d/m/Y"), out.format = c(dates = "Y_m_d")), "_to_", chron(KC_SEQ$enddate[idseq_index], format=c(dates = "d/m/Y"), out.format = c(dates = "Y_m_d")), "_")
  
  
  if (idseq_index==1) {
    KC_SEQ$padding[idseq_index] <- as.integer(chron(KC_SEQ$startdate[idseq_index], format=c(dates = "d/m/Y")) - chron(DEBUT_SIMULATION, format=c(dates = "Y-m-d")))
  } else {
    KC_SEQ$padding[idseq_index] <- as.integer(chron(KC_SEQ$startdate[idseq_index], format=c(dates = "d/m/Y")) - chron(KC_SEQ$enddate[idseq_index-1], format=c(dates = "d/m/Y")))
  }
  {
    if (KC_SEQ$padding[idseq_index]<1) {
      if (idseq_index==1) {
        cat("Attention date de debut de la séquence avant le début de simulation\n")
        cat(as.character(chron(DEBUT_SIMULATION, format=c(dates = "Y-m-d"), out.format=c(dates = "d/m/Y"))), " < ", as.character(chron(KC_SEQ$startdate[idseq_index], format=c(dates = "d/m/Y"))), "\n")
      } else {
        cat("Attention date de debut avant date de fin du précédent\n")
        cat(as.character(chron(KC_SEQ$startdate[idseq_index], format=c(dates = "d/m/Y"))), " < ", as.character(chron(KC_SEQ$enddate[idseq_index-1], format=c(dates = "d/m/Y"))), "\n",
            idseq_index, " vs ", idseq_index-1, " (element de la séquence)\n")
      }
      error_kc <- TRUE;
    }
    dureeDates <- as.integer(chron(KC_SEQ$enddate[idseq_index], format=c(dates = "d/m/Y")) - chron(KC_SEQ$startdate[idseq_index], format=c(dates = "d/m/Y")))
    dureePaires <- max(kc_list[[id_crop]]$date)
    if (dureeDates!=dureePaires) {
      cat("Attention durée différentes entre dates et fichiers de kc\n")
      cat(idseq_index, "eme element de la séquence, ID:", id_crop, "\n")
      cat(dureeDates, "(dates séquence) != ", dureePaires, "(durée paires)\n")
      error_kc <- TRUE;
    }
  }
  padding <- padding+KC_SEQ$padding[idseq_index]
  for (idpair_index in 1:nrow(kc_list[[id_crop]])) {#idpair_index=1
    kc_list[[id_crop]][idpair_index,]$kc
    as.integer(kc_list[[id_crop]][idpair_index,]$date + padding)
    kc_pair_index <- kc_pair_index+1
  }
  
  padding <- padding+max(kc_list[[id_crop]]$date)
  
  
  stage_dates <- date_debut+as.integer(kc_list[[id_crop]]$date + padding)
  
  #extraction des dates de debut et fin de stades (attend un fichier de paires avec 8 lignes)
  kcdates_list[[id_crop]] <- list(ini_startdate=stage_dates[2], 
                                                       ini_enddate=stage_dates[3], 
                                                       mid_startdate=stage_dates[4], 
                                                       mid_enddate=stage_dates[5], 
                                                       late_startdate=stage_dates[6], 
                                                       late_enddate=stage_dates[7]);
  
  #calul des moyennes climatiques par phase pheno (ini, mid, late)
  RHmin_mean[[id_crop]] <- list(ini=mean(weatherdata$RHmin[(weatherdates>=kcdates_list[[id_crop]]$ini_startdate) & (weatherdates<=kcdates_list[[id_crop]]$ini_enddate)]),
                                                     mid=mean(weatherdata$RHmin[(weatherdates>=kcdates_list[[id_crop]]$mid_startdate) & (weatherdates<=kcdates_list[[id_crop]]$mid_enddate)]), 
                                                     late=mean(weatherdata$RHmin[(weatherdates>=kcdates_list[[id_crop]]$late_startdate) & (weatherdates<=kcdates_list[[id_crop]]$late_enddate)]));
  
  u2_mean[[id_crop]] <- list(ini=mean(weatherdata$u2[(weatherdates>=kcdates_list[[id_crop]]$ini_startdate) & (weatherdates<=kcdates_list[[id_crop]]$ini_enddate)]),
                                                      mid=mean(weatherdata$u2[(weatherdates>=kcdates_list[[id_crop]]$mid_startdate) & (weatherdates<=kcdates_list[[id_crop]]$mid_enddate)]), 
                                                      late=mean(weatherdata$u2[(weatherdates>=kcdates_list[[id_crop]]$late_startdate) & (weatherdates<=kcdates_list[[id_crop]]$late_enddate)]));
  
  h[[id_crop]] <- all_crop_params$h[all_crop_params$CropID==id_crop]
  
  
  kc_ini <- kc_ini_CC <- kc_list[[id_crop]]$kc[2];
  kc_mid <- kc_mid_CC <- kc_list[[id_crop]]$kc[4];
  kc_late <- kc_late_CC <- kc_list[[id_crop]]$kc[6];
  
  
  if (kc_ini>0.45) {
    kc_ini_CC <- kc_ini+(0.04*(u2_mean[[id_crop]]$ini-2.)-0.004*(RHmin_mean[[id_crop]]$ini-45.))*(h[[id_crop]]/3.)^0.3
  }
  if (kc_mid>0.45) {
    kc_mid_CC <- kc_mid+(0.04*(u2_mean[[id_crop]]$mid-2.)-0.004*(RHmin_mean[[id_crop]]$mid-45.))*(h[[id_crop]]/3.)^0.3
  }
  if (kc_late>0.45) {
    kc_late_CC <- kc_late+(0.04*(u2_mean[[id_crop]]$late-2.)-0.004*(RHmin_mean[[id_crop]]$late-45.))*(h[[id_crop]]/3.)^0.3
  }

  new_crop_id <- paste0(id_crop, "_", CLIM_PREFIX, DATE_PREFIX);
  
  
  new_kc_list[[new_crop_id]] <- new_kc_list[[id_crop]]
  new_kc_list[[new_crop_id]]$kc[2:3] <- kc_ini_CC
  new_kc_list[[new_crop_id]]$kc[4:5] <- kc_mid_CC
  new_kc_list[[new_crop_id]]$kc[6:7] <- kc_late_CC
  
  all_crop_params <- rbind(all_crop_params, data.frame(CropID=new_crop_id, 
                                                       h=all_crop_params$h[all_crop_params$CropID==id_crop], 
                                                       MaxDepthRoots=all_crop_params$MaxDepthRoots[all_crop_params$CropID==id_crop],
                                                       kcini=all_crop_params$kcini[all_crop_params$CropID==id_crop],
                                                       kcmid=all_crop_params$kcmid[all_crop_params$CropID==id_crop],
                                                       IrrigationDose=all_crop_params$IrrigationDose[all_crop_params$CropID==id_crop],
                                                       Max_Irrigation_Duration=all_crop_params$Max_Irrigation_Duration[all_crop_params$CropID==id_crop],
                                                       Rain_Threshold=all_crop_params$Rain_Threshold[all_crop_params$CropID==id_crop],
                                                       SoilWaterContent_Threshold=all_crop_params$SoilWaterContent_Threshold[all_crop_params$CropID==id_crop],
                                                       CropWaterStress_Threshold=all_crop_params$CropWaterStress_Threshold[all_crop_params$CropID==id_crop]))
  
  all_crop_params$kcini[all_crop_params$CropID==new_crop_id] <- kc_ini_CC;
  all_crop_params$kcmid[all_crop_params$CropID==new_crop_id] <- kc_mid_CC;
  
  
  newkc_filename <- file.path(KC_DATADIR, paste0(NEWFILES_PREFIX, CLIM_PREFIX, DATE_PREFIX, KC_DATA$filename[KC_DATA$id==id_crop]));
  cat("Writing new kc pairs file (for crop id: '", new_crop_id, "'): ", newkc_filename, "\n", sep = "");
  write_csv2(new_kc_list[[new_crop_id]], newkc_filename);
  
  new_KC_DATA <- rbind(new_KC_DATA, c(id=new_crop_id, filename=basename(newkc_filename)));
  
  KC_SEQ$sequence[idseq_index] <- new_crop_id;
}

newcropparam_filename <- file.path(dirname(CROP_DATAFILE), paste0(NEWFILES_PREFIX, CLIM_PREFIX, basename(CROP_DATAFILE)));
cat("Writing new crop parameters file: ", newcropparam_filename, "\n", sep = "")
write_csv2(all_crop_params, newcropparam_filename)





cat("\n\nOptions to use in next script:\n")

cat("DEBUT_SIMULATION <- '", DEBUT_SIMULATION, "';\n", sep="")
cat("FICHIER_CLIMAT <- '", FICHIER_CLIMAT, "';\n", sep="")
cat("CROP_DATAFILE <- '", newcropparam_filename, "';\n", sep="")

cat("KC_DATADIR <- '", KC_DATADIR, "';\n", sep="")

cat("KC_DATA <- ", sep="");
dput(new_KC_DATA)

cat("KC_T0 <- ", KC_T0, ";\n", sep="")

cat("KC_SEQ <- ", sep="");
dput(KC_SEQ)

